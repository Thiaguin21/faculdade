
//INCLUI AS BIBLIOTECAS UTILIZADAS E DEFINE AS CONSTANTES
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include "beale.h"
#define SIZE 100


//FUNÇÃO QUE BUSCA UM CARACTER NA LISTA DE CARACTERES
struct nodo_caracter* busca_caractere(struct lista_caracteres* l, char c){
    struct nodo_caracter* aux;
    aux = l->ini;

    while(aux != NULL){
        if(aux->caracter == tolower(c))
            return aux;
        aux = aux->prox;
    }

    return NULL;
}


//FUNÇÃO QUE GERA ESCOLHE RANDÔMICAMENTE UMA CIFRA RESPECTIVA À UM CARACTER
unsigned long gera_chave_randomica(struct lista_chaves* l){
    struct nodo_chave* aux;
    int num_rand, max = l->tam, min = 0;

    num_rand = rand() % (max - min + 1) + min;
    aux = l->ini;

    for(int i = 0; i < num_rand - 1; ++i){
        aux = aux->prox;
    }
    return aux->chave;
}


//REALIZA A CODIFICAÇÃO DE UMA MENSAGEM
int codifica(FILE* mensagem_original, FILE* mensagem_codificada, struct lista_caracteres* l){
    struct nodo_caracter* n;
    char leitura;
    unsigned long chave;
    srand(time(0));

    leitura = fgetc(mensagem_original);
    while(!feof(mensagem_original)){
        if(leitura == '\n')
            fprintf(mensagem_codificada, "-3 ");
        else if(leitura == ' ')
            fprintf(mensagem_codificada, "-2 ");
        else{
            n = busca_caractere(l ,leitura);            
            if(n == NULL)
                fprintf(mensagem_codificada, "-1 ");
            else{
                chave = gera_chave_randomica(n->chaves);
                fprintf(mensagem_codificada, "%ld ", chave);
            }
        }
        leitura = fgetc(mensagem_original);
    }  

    return 1;
}
