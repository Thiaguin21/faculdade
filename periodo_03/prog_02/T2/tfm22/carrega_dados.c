
//INCLUI AS BIBLIOTECAS UTILIZADAS E DEFINE AS CONSTANTES
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "beale.h"
#define SIZE 100


//FUNÇÃO PARA CRIAR A LISTA DE CARACTERES
struct lista_caracteres* cria_lista_caracteres(){
    
    struct lista_caracteres* l;
    if(!(l = malloc(sizeof(struct lista_caracteres))))
        return 0;
    
    l->ini = NULL;
    l->fim = l->ini;
   
    return l;
}


//FUNÇÃO QUE CRIA A LISTA DE CIFRAS VAZIA
struct lista_chaves* cria_lista_chaves(){
    
    struct lista_chaves* l;
    if(!(l = malloc(sizeof(struct lista_chaves))))
        return 0;
    
    l->ini = NULL;
    l->fim = NULL;
    l->tam = 0;
   
    return l;
}


//FUNÇÃO QUE CRIA UM CARACTER E A SUA PRIMEIRA CIFRA
struct nodo_caracter* cria_nodo_caracter(char c, unsigned long num, struct nodo_caracter* prox){
    struct nodo_caracter* n;
    
    if(!(n = malloc(sizeof(struct nodo_caracter))))
        return NULL;

    n->caracter = tolower(c);
    n->chaves = cria_lista_chaves();
    n->prox = prox;
    insere_lista_chaves(n->chaves, num);

    return n;
}


//FUNÇÃO QUE DESTRÓI A ESTRUTURA DE DADOS DE CIFRAS DE UM ÚNICO CARACTER
struct lista_chaves* destroi_lista_chaves(struct lista_chaves* l){
    struct nodo_chave* aux1;

    while(l->ini != l->fim){
        aux1 = l->ini;
        l->ini = aux1->prox;
        free(aux1);
    }
    free(l->fim);
    free(l);

    return NULL;
}


//FUNÇÃO PARA DESTRUIR A LISTA DE CARACTERES
struct lista_caracteres* destroi_lista_caracteres(struct lista_caracteres* l){
    struct nodo_caracter* aux1;

    if(l->ini != NULL){
        while(l->ini != l->fim){
            aux1 = l->ini;
            l->ini = aux1->prox;
            destroi_lista_chaves(aux1->chaves);
            free(aux1);
        }
        free(l->fim);
    }
    free(l);

    return NULL;
}


//FUNÇÃO QUE INSERE UMA NOVA CIFRA À LISTA DE CIFRAS DE UM CARACTER
int insere_lista_chaves(struct lista_chaves* l, unsigned long chave){
    struct nodo_chave* n;

    if(!(n = malloc(sizeof(struct nodo_chave))))
        return 0;
    
    n->chave = chave;
    n->prox = NULL;
    l->tam += 1;

    if(l->ini == NULL){
        l->ini = n;
        l->fim = l->ini;
        return 1;
    }

    (l->fim)->prox = n; 
    l->fim = (l->fim)->prox;

    return 1;
}


//FUNÇÃO QUE INSERE UM NOVO CARACTER À LISTA DE CARACTERES
int insere_lista_caracteres(struct lista_caracteres* l, char c, unsigned long num){
    struct nodo_caracter* aux1;
    struct nodo_caracter* aux2;
    
    //LISTA VAZIA
    if(l->ini == NULL){
        l->ini = cria_nodo_caracter(c,num,NULL);
        return 1;
    }

    aux1 = l->ini;
    if(tolower(aux1->caracter) > tolower(c)){
        l->ini = cria_nodo_caracter(c,num,aux1);
        return 1;
    }

    else if(tolower(aux1->caracter) == tolower(c)){
        insere_lista_chaves(aux1->chaves, num);
        return 1;
    }

    aux2 = aux1->prox;
    if(aux1->prox == NULL){
        aux1->prox = cria_nodo_caracter(c,num,NULL);
        return 1;
    }

    while(aux2->prox != NULL){
        
        if(tolower(aux2->caracter) > tolower(c)){
            aux1->prox = cria_nodo_caracter(c,num,aux2);
            return 1;
        }

        if(tolower(aux2->caracter) == tolower(c)){
            insere_lista_chaves(aux2->chaves, num);
            return 1;
        }

        aux1 = aux2;
        aux2 = aux2->prox;
    }

    if(tolower(aux2->caracter) == tolower(c)){
            insere_lista_chaves(aux2->chaves, num);
            return 1;
    }

    aux2->prox = cria_nodo_caracter(c,num,NULL);
    
    return 1;
}


//FUNÇÃO QUE GERA A LISTA DE CHAVES A PARTIR DE UM LIVRO CIFRA
void gera_lista_chaves(FILE* livro_cifras, struct lista_caracteres* l){
    unsigned long i = 0;
    char leitura[SIZE];

    fscanf(livro_cifras, "%s", leitura);
    while(!feof(livro_cifras)){
        for (int j = 0; j < strlen(leitura); ++j){
            if(isprint(leitura[j])){
                insere_lista_caracteres(l, leitura[j], i);
                break;    
            }
        }
        
        ++i;
        fscanf(livro_cifras, "%s", leitura);
    }

    return;
}


//FUNÇÃO QUE CONVERTE UM ARQUIVO DE CHAVES PARA UMA LISTA DE CHAVES
void carrega_arquivo_chaves(FILE* arquivo_chaves, struct lista_caracteres* l){
    char chaves[SIZE];
    char letra;
    char leitura;
    unsigned long chave;

    memset(chaves, '\0', sizeof(char)*SIZE);
    letra = fgetc(arquivo_chaves);
    while(!feof(arquivo_chaves)){
        
        leitura = fgetc(arquivo_chaves);
        while(!isdigit(leitura))
            leitura = fgetc(arquivo_chaves);
        
        while(!feof(arquivo_chaves)){
            int i=0;
            while(leitura != ' ' && leitura != '\n' && !feof(arquivo_chaves)){
                chaves[i] = leitura;
                ++i;
                leitura = fgetc(arquivo_chaves);    
            }
            chave = strtoul(chaves, NULL, 10);
            insere_lista_caracteres(l, letra, chave);

            memset(chaves, '\0', sizeof(char)*SIZE);
            if(leitura == '\n')
                break;    
            leitura = fgetc(arquivo_chaves);
        }

        if(feof(arquivo_chaves))
            break;
        letra = fgetc(arquivo_chaves);
    }  

    return;
}
