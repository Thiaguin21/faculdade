
//BIBLIOTECA PARA USUÁRIOS 
/*  Nessa biblioteca encontram-se apenas as funções que usuário terá acesso
 */


//INCLUI A BIBLIOTECA COM AS FUNÇÕES DE MANIPULAÇÃO DA ESTRUTURA DE DADOS
#include <stdlib.h>
#include "backend_beale.h"


//FUNÇÃO PARA CRIAR A LISTA DE CARACTERES
/*  Essa função cria uma lista estrutura de dados vazia para o armazenamento dos dados
 */
struct lista_caracteres* cria_lista_caracteres();


//FUNÇÃO PARA DESTRUIR A LISTA DE CARACTERES
/*  Essa função destrói toda a estrutura de dados e suas ramificações
 */
struct lista_caracteres* destroi_lista_caracteres(struct lista_caracteres* l);


//FUNÇÃO QUE GERA A LISTA DE CHAVES A PARTIR DE UM LIVRO CIFRA
/*  Realiza a leitura de um livro cifra e cria as cifras para a codificação
 *  A codificação se dá pela posição de cada palavra, sendo codificado apenas o caracter inicial de cada palavra  
 */
void gera_lista_chaves(FILE* livro_cifras, struct lista_caracteres* l);


//FUNÇÃO QUE CONVERTE UM ARQUIVO DE CHAVES PARA UMA LISTA DE CHAVES
/*  Realiza a leitura de um arquivo com as cifras já geradas e traduz as cifras para a estrutura de dados
 *  A codificação se dá pela posição de cada palavra, sendo codificado apenas o caracter inicial de cada palavra  
 */
void carrega_arquivo_chaves(FILE* arquivo_chaves, struct lista_caracteres* l);


//FUNÇÃO QUE GERA UM ARQUIVO DE CHAVES
/*  Converte a estrutura de dados gerada a parir de um livro cifra para um arquivo
 */
void gera_arquivo_chaves(FILE* arquivo_chaves, struct lista_caracteres* l);


//REALIZA A CODIFICAÇÃO DE UMA MENSAGEM
/*  Realiza a codificação da mensagem utilizando as cifras geradas a partir de um livro cifra
 */
int codifica(FILE* mensagem_original, FILE* mensagem_codificada, struct lista_caracteres* l);


//REALIZA A DECODIFICAÇÃO DE UMA MENSAGEM CODIFICADA
/*  Realiza a decodificação de uma mensagem codificada, a decodificação poderá ocorrer de duas formas:
 *  Utilizando um livro cifra
 *  Utilizando um arquivp de chaves
 */
int decodifica(FILE* mensagem_codificada, FILE* mensagem_decodificada, struct lista_caracteres* l);
