
//INCLUI AS BIBLIOTECAS UTILIZADAS
#include <stdio.h>
#include <stdlib.h>
#include "beale.h"


//FUNÇÃO QUE GERA UM ARQUIVO DE CHAVES
void gera_arquivo_chaves(FILE* arquivo_chaves, struct lista_caracteres* l){
        struct nodo_caracter* aux1;
        struct nodo_chave* aux2;
        struct lista_chaves* chaves;
        aux1 = l->ini;

        while(aux1 != NULL){
            fprintf(arquivo_chaves, "%c: ",aux1->caracter);
            chaves = aux1->chaves;
            aux2 = chaves->ini;
            while(aux2 != chaves->fim){
                fprintf(arquivo_chaves, "%ld ",aux2->chave);
                aux2 = aux2->prox;
            }
            fprintf(arquivo_chaves, "%ld\n",(chaves->fim)->chave);
            aux1 = aux1->prox;
        }
        
        return;
}
