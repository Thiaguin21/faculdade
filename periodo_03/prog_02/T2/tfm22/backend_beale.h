
/*  BIBLIOTECA ONDE ESTÃO AS FUNÇÕES DE MANIPULAÇÃO DA ESTRUTURA DE DADOS
 *  O usuário da beale.h não terá acesso à essa funções, não podendo modificar a estrutura de dados
 *  A funcionalidade dessa bibliteca é a garantia das funcionalidades
 */

//INCLUI AS BIBLIOTECAS UTILIZADAS
#include <stdio.h>


//COMPONENTE DA ESTRUTURA DE DADOS DE CIFRAS
/*  chave => variável onde ficará armazenada a cifra
 *  prox => ponteiro que aponta para a próxima cifra 
 */
struct nodo_chave {
    unsigned long chave;
    struct nodo_chave* prox;
};


//ESTRUTURA DE DADOS PARA O ARMAZENAMENTO DAS CIFRAS
/*  ini => ponteiro que aponta para o início da lista de cifras, ou seja para a primeira cifra
 *  fim => ponteiro que aponta para o fim da lista de cifras, ou seja para a última cifra
 *  tam => variável para armazenar o tamanho da lista
 */
struct lista_chaves {
    struct nodo_chave* ini;
    struct nodo_chave* fim;
    int tam;
};


//COMPONENTE DA ESTRUTURA DE DADOS DE CARACTERES
/*  caracter => variável onde ficará armazenado o caracter
 *  chaves => ponteiro para a lista de cifras com as respectivas cifras do caracter
 *  prox => ponteiro que aponta para o próximo caracter
 */
struct nodo_caracter {
    char caracter;
    struct lista_chaves* chaves;
    struct nodo_caracter* prox;
};


//ESTRUTURA DE DADOS PARA O ARMAZENAMENTO DOS CARACTERES
/*  ini => ponteiro que aponta para o início da lista de caracteres, ou seja para o primeiro caracter
 *  fim => ponteiro que aponta para o fim da lista de caracteres, ou seja para o último caracter
 */
struct lista_caracteres {
    struct nodo_caracter* ini;
    struct nodo_caracter* fim;
};


//FUNÇÃO QUE CRIA A LISTA DE CIFRAS VAZIA
struct lista_chaves* cria_lista_chaves();


//FUNÇÃO QUE CRIA UM CARACTER E A SUA PRIMEIRA CIFRA
struct nodo_caracter* cria_nodo_caracter(char c, unsigned long num, struct nodo_caracter* prox);


//FUNÇÃO QUE DESTRÓI A ESTRUTURA DE DADOS DE CIFRAS DE UM ÚNICO CARACTER
struct lista_chaves* destroi_lista_chaves(struct lista_chaves* l);


//FUNÇÃO QUE INSERE UMA NOVA CIFRA À LISTA DE CIFRAS DE UM CARACTER
/*  A inserção da nova cifra ocorre sempre no final da lista de cifras
 *  Isso ocorre pelo fato do algoritmo gerador de cifras realizar a geração das cifras de forma gradual e crescente
 *  Ou seja, garantimos assim que a lista de cifras sempre estará contruída seguindo tal metodologia
 */
int insere_lista_chaves(struct lista_chaves* l, unsigned long chave);


//FUNÇÃO QUE INSERE UM NOVO CARACTER À LISTA DE CARACTERES
/*  A inserção do novo caracter ocorre de forma ordenada
 *  Se o caracter já existir na lista de caracteres, apenas será adicionada a cifra na lista de cifras referente à esse caracter
 */
int insere_lista_caracteres(struct lista_caracteres* l, char c, unsigned long num);


//FUNÇÃO QUE BUSCA UM CARACTER NA LISTA DE CARACTERES
/*  A busca realizada é uma busca ingênua
 *  Ou seja, no pior caso irá percorrer completamente a lista de caracteres
 */
struct nodo_caracter* busca_caractere(struct lista_caracteres* l, char c);


//FUNÇÃO QUE GERA ESCOLHE RANDÔMICAMENTE UMA CIFRA RESPECTIVA À UM CARACTER
/*  Utiliza como o seguinte intervalo [0 .. tam-1], sendo "tam" o tamanho da lista de cifras 
 *  Gera randômicamente um valor entre esse intervalo e cria um laço incremental até a cifra a ser escolhida
 */
unsigned long gera_chave_randomica(struct lista_chaves* l);


//FUNÇÃO QUE BUSCA UMA CIFRA NA LISTA DE CIFRAS DE UM CARACTER
/*  A busca realizada é uma busca ingênua
 *  Ou seja, no pior caso irá percorrer completamente a lista de cifras
 */
struct nodo_caracter* busca_chave(struct lista_caracteres* l, unsigned long chave);
