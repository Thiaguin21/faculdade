
//INCLUI AS BIBLIOTECAS UTILIZADAS E DEFINE AS CONSTANTES
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "beale.h"
#define SIZE 100


//FUNÇÃO QUE BUSCA UMA CIFRA NA LISTA DE CIFRAS DE UM CARACTER
struct nodo_caracter* busca_chave(struct lista_caracteres* l, unsigned long chave){
    struct nodo_caracter* aux1;
    struct nodo_chave* aux2;
    aux1 = l->ini;

    while(aux1 != NULL){
        aux2 = (aux1->chaves)->ini;
        while(aux2 != NULL){
            if(aux2->chave == chave)
                return aux1;
            aux2 = aux2->prox;
        }
        aux1 = aux1->prox;
    }

    return NULL;
}


//REALIZA A DECODIFICAÇÃO DE UMA MENSAGEM CODIFICADA
int decodifica(FILE* mensagem_codificada, FILE* mensagem_decodificada, struct lista_caracteres* l){
    struct nodo_caracter* n;
    char palavra[SIZE];
    char leitura;
    unsigned long chave;

    memset(palavra, '\0', sizeof(char)*SIZE);
    leitura = fgetc(mensagem_codificada);
    while(!feof(mensagem_codificada)){
        int i = 0;
        while(leitura != ' ' && !feof(mensagem_codificada)){
            palavra[i] = leitura;
            ++i;
            leitura = fgetc(mensagem_codificada);
        }

        if(palavra[0] != '-'){
            chave = strtoul(palavra, NULL, 10);
            n = busca_chave(l, chave);
            fprintf(mensagem_decodificada, "%c", n->caracter);

        }
        else if(palavra[0] == '-'){
            if(palavra[1] == '1')
                fprintf(mensagem_decodificada, "?");
            else if(palavra[1] == '2')
                fprintf(mensagem_decodificada, " ");
            else if(palavra[1] == '3')
                fprintf(mensagem_decodificada, "\n");
        }

        memset(palavra, '\0', sizeof(char)*SIZE);
        leitura = fgetc(mensagem_codificada);
    }  

    return 1;
}
