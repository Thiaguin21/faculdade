
//INCLUI AS BIBLIOTECAS E FUNÇÕES QUE SERÃO UTILIZADAS
#include "beale.h"
int getopt(int argc, char * const argv[], const char* optstring);
extern char* optarg;

//FUNÇÃO PRINCIPAL
int main(int argc, char *argv[]){
	FILE *mensagem_original, *mensagem_codificada, *mensagem_decodificada, *livro_cifras, *arquivo_chaves;
	int flag_e = 0, flag_d = 0, flag_b = 0, flag_c = 0, flag_m = 0, flag_i = 0, flag_o = 0;
	char *livro_cifra, *arq_chaves, *msg_orig, *msg_cod, *arq_criar;
  	int parametro;
  	struct lista_caracteres* l;

  	//CONFERE OS PARÂMETROS RECEBIDOS
  	while((parametro = getopt(argc, argv, "edb:c:m:i:o:")) != -1){
    	switch (parametro){
    		case 'e':		//CONFERE SE O PARÂMETRO DE CODIFICAÇÃO FOI RECEBIDO
        		flag_e = 1;
        		break;
      		case 'd':		//CONFERE SE O PARÂMETRO DE DECODIFICAÇÃO FOI RECEBIDO
        		flag_d = 1;
        		break;
    		case 'b':		//CONFERE SE O PARÂMETRO DO LIVRO CIFRA FOI RECEBIDO
        		flag_b = 1;
        		livro_cifra = optarg;
        		break;
        	case 'c':		//CONFERE SE O PARÂMETRO DO ARQUIVO DE CHAVES FOI RECEBIDO
        		flag_c = 1;
        		arq_chaves = optarg;
        		break;
    		case 'm':		//CONFERE SE O PARÂMETRO DA MENSAGEM ORIGINAL FOI RECEBIDO
        		flag_m = 1;
        		msg_orig = optarg;
        		break;
        	case 'i':		//CONFERE SE O PARÂMETRO DA MENSAGEM CODIFICADA FOI RECEBIDO
        		flag_i = 1;
        		msg_cod = optarg;
        		break;
        	case 'o':		//CONFERE SE O PARÂMETRO PARA A CRIAÇÂO DE UM AEQUIVO DE DESTINO FOI RECEBIDO
        		flag_o = 1;
        		arq_criar = optarg;
        		break;
    		default:		//CONFERE ALGUM PArÂMETRO INVÁLIDO FOI RECEBIDO
        		printf("ERRO! Os parametros passados estão incorretos\n");
				exit (1) ;
      	}
    }

    //PROGRAMA NÃO CONFIGURADO OU CONFIGURADO INCORRETAMENTE 
    if((flag_e && flag_d) || (!flag_e && !flag_d)){
    	printf("Erro! O programa não foi configurado para codificar/decodificar ou foi configurado com ambos\n");
	    exit(1);
    }

    //PROGRAMA CONFIGURADO PARA CODIFICAÇÂO
    else if(flag_e){

    	//PASSAGEM DO PARÂMETRO CORRESPONDENTE À DECODIFICAÇÃO
    	if(flag_i){
    		printf("Erro! Parâmetros incorretos\n");
	    	exit(1);
    	}

        //CRIA A ESTRUTURA ONDE FICARÃO ARMAZENADOS OS DADOS
    	l = cria_lista_caracteres();

    	//SEM ENTRADAS DE ARQUIVOS PORÉM PODE GERAR O ARQUIVO DE CIFRAS
    	if(!flag_m || !flag_o){
    		
    		//TESTA SE É POSSÍVEL GERAR O ARQUIVO DE CHAVES	    	
	    	if(!flag_b || !flag_c){
	    		printf("Erro! Arquivos de entrada insuficientes\n");
    			l = destroi_lista_caracteres(l);
		    	exit(1);
	    	}

	    	//APENAS GERA O ARQUIVO DE CHAVES
	    	else{
	  		
	  			//ABRE O ARQUIVO DO LIVRO DE CIFRAS
	    	    livro_cifras = fopen(livro_cifra,"r");
			    if(!livro_cifras){
			        printf("Erro ao abrir o arquivo %s\n", livro_cifra);
			        l = destroi_lista_caracteres(l);
			        exit(1);
			    }

			    //GERA AS CIFRAS A PARTIR DO LIVRO DE CIFRAS
			    gera_lista_chaves(livro_cifras, l);
			    fclose(livro_cifras);

			    //CRIA O ARQUIVO À SEREM INSERIDAS AS CIFRAS
		    	arquivo_chaves = fopen(arq_chaves, "w");
		    	if(!arquivo_chaves){
			        printf("Erro ao criar o arquivo %s\n", arq_chaves);
			        l = destroi_lista_caracteres(l);
			        exit(1);
		    	}

		    	//GERA O ARQUIVO DE CIFRAS
		    	gera_arquivo_chaves(arquivo_chaves, l);
		    	fclose(arquivo_chaves);
		    	l = destroi_lista_caracteres(l);
		    	return 0;
			}
	    }

	    //ENTRADAS DE ARQUIVOS VÁLIDA
	    else{

	    	//ABRE O ARQUIVO COM A MENSAGEM ORIGINAL
		    mensagem_original = fopen(msg_orig, "r");
		    if(!mensagem_original){
		        printf("Erro ao abrir o arquivo %s\n", msg_orig);
		        l = destroi_lista_caracteres(l);
		        exit(1);
		    }

			//SEM ENTRADA DE DADOS PARA A CODIFICAÇÃO	    	
	    	if(!flag_b){
	    		printf("Erro! Não foi passado o arquivo do livro de cifras para a geração das chaves a serem utilizadas na codificação\n");
    			fclose(mensagem_original);
    			l = destroi_lista_caracteres(l);
		    	exit(1);
	    	}

	    	//CODIFICAÇÃO UTILIZANDO UM LIVRO CIFRA
	    	else{
	  		
	  			//ABRE O ARQUIVO DO LIVRO DE CIFRAS
	    	    livro_cifras = fopen(livro_cifra,"r");
			    if(!livro_cifras){
			        printf("Erro ao abrir o arquivo %s\n", livro_cifra);
			        fclose(mensagem_original);
			        l = destroi_lista_caracteres(l);
			        exit(1);
			    }

			    //GERA AS CIFRAS A PARTIR DO LIVRO DE CIFRAS
			    gera_lista_chaves(livro_cifras, l);
			    fclose(livro_cifras);

		    	//CRIA O ARQUIVO À SER INSERIDA A MENSAGEM CODIFICADA
			    mensagem_codificada = fopen(arq_criar, "w");
			    if(!mensagem_codificada){
			        printf("Erro ao criar o arquivo %s\n", arq_criar);
			        fclose(mensagem_original);
			        l = destroi_lista_caracteres(l);
			        exit(1);
			    }

	    		//REALIZA A CODIFICAÇÃO DA MENSAGEM ORIGINAL E SALVA  A MENSAGEM CODIFICADA EM UM ARQUIVO DE DESTINO 
	    		codifica(mensagem_original, mensagem_codificada, l);
	    		fclose(mensagem_original);
			    fclose(mensagem_codificada);

	  			//GERA ARQUIVO DE CHAVES
	    		if(flag_c){

	    			//CRIA O ARQUIVO À SEREM INSERIDAS AS CIFRAS
			    	arquivo_chaves = fopen(arq_chaves, "w");
			    	if(!arquivo_chaves){
				        printf("Erro ao criar o arquivo %s\n", arq_chaves);
				        fclose(livro_cifras);
				        l = destroi_lista_caracteres(l);
				        exit(1);
			    	}
			    	gera_arquivo_chaves(arquivo_chaves, l);
			    	fclose(arquivo_chaves);
	    		}

	    		l = destroi_lista_caracteres(l);
	    		return 0;
	    	}
	    }
    }

    //PROGRAMA CONFIGURADO PARA DECODIFICAÇÂO
    else if(flag_d){

    	//PASSAGEM DO PARÂMETRO CORRESPONDENTE À CODIFICAÇÃO
    	if(flag_m){
    		printf("Erro! Parâmetros incorretos\n");
	    	exit(1);
    	}

        //CRIA A ESTRUTURA ONDE FICARÃO ARMAZENADOS OS DADOS
    	l = cria_lista_caracteres();

    	//SEM ENTRADAS DE ARQUIVOS OU ENTRADA INVÁLIDA
    	if(!flag_i || !flag_o){
    		printf("Erro! Não foram passados corretamente os arquivos de origem e de destino para a decodificação\n");
    		l = destroi_lista_caracteres(l);
		    exit(1);
    	}

    	//ENTRADAS DE ARQUIVOS VÁLIDA
    	else{

    		//ABRE O ARQUIVO COM A MENSAGEM CODIFICADA
    	    mensagem_codificada = fopen(msg_cod, "r");
		    if(!mensagem_codificada){
		        printf("Erro ao abrir o arquivo %s\n", msg_cod);
		        l = destroi_lista_caracteres(l);
		        exit(1);
		    }

		    //SEM ENTRADAS DE ARQUIVOS DE CIFRAS OU AMBOS SELECIONADOS
    		if((!flag_b && !flag_c) || (flag_b && flag_c)){
				printf("Erro! Não foram passados ou foram passados ambos os arquivos de cifras\n");
    			l = destroi_lista_caracteres(l);
    			fclose(mensagem_codificada);
		    	exit(1);
    		}

    		//DECODIFICAÇÃO UTILIZANDO UM LIVRO CIFRA
    		else if(flag_b){

				//ABRE O ARQUIVO DO LIVRO DE CIFRAS
	    	    livro_cifras = fopen(livro_cifra,"r");
			    if(!livro_cifras){
			        printf("Erro ao abrir o arquivo %s\n", livro_cifra);
			        fclose(mensagem_codificada);
			        l = destroi_lista_caracteres(l);
			        exit(1);
			    }

			    //GERA AS CIFRAS A PARTIR DO LIVRO DE CIFRAS
			    gera_lista_chaves(livro_cifras, l);
			    fclose(livro_cifras);
    		}

    		//DECOFICAÇÃO UTILIZANDO UM ARQUIVO DE CHAVES
    		else if(flag_c){

    			//ABRE O ARQUIVO DE CIFRAS
    			arquivo_chaves = fopen(arq_chaves, "r");
			    if(!arquivo_chaves){
			        printf("Erro ao abrir o arquivo %s\n", arq_chaves);
			        fclose(mensagem_codificada);
			        l = destroi_lista_caracteres(l);
			        exit(1);
			    }

			    //CARREGA AS CIFRAS CONTIDAS NO ARQUIVO DE CIFRAS
			    carrega_arquivo_chaves(arquivo_chaves, l);
			    fclose(arquivo_chaves);
    		}

    		//CRIA O ARQUIVO À SER INSERIDA A MENSAGEM DECODIFICADA
		    mensagem_decodificada = fopen(arq_criar, "w");
		    if(!mensagem_decodificada){
		        printf("Erro ao criar o arquivo %s\n", arq_criar);
		        fclose(mensagem_codificada);
		        l = destroi_lista_caracteres(l);
		        exit(1);
		    }

		    //REALIZA A DECODIFICAÇÃO DA MENSAGEM CODIFICADA E SALVA  A MENSAGEM DECODIFICADA EM UM ARQUIVO DE DESTINO
		    decodifica(mensagem_codificada, mensagem_decodificada, l);
		    fclose(mensagem_codificada);
		    fclose(mensagem_decodificada);
		    l = destroi_lista_caracteres(l);
    		return 0;
    	}
    }
    
    return 0;
}