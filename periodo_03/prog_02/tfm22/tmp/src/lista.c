
// BIBLIOTECAS UTILIZADAS
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <grp.h>
#include <pwd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include "vpp.h"


// FUNÇÃO QUE CRIA A LISTA DE MEMBROS VAZIA
struct lista *criaListaMembros(char *arquivo)
{
    struct lista *l;

    l = calloc(sizeof(struct lista), 1);

    if(l == NULL)
        return NULL;

    strcpy(l->arquivo, arquivo);

    l->diretorios = sizeof(int) + sizeof(long int);

    return l;
}


// FUNÇÃO QUE DESTROI A LISTA DE MEMBROS
struct lista *destroiListaMembros(struct lista *l)
{    
    struct membro *aux;

    while(l->ini != NULL){
        aux = l->ini;
        l->ini = aux->prox;
        free(aux);
    }
    
    free(l);
    
    return NULL;
}


// FUNÇÃO QUE CARREGA OS DADOS DE UM MEMBRO DO ARQUIVO VPP
struct membro *leDados(FILE *arq)
{
    struct membro *m;

    m = calloc(sizeof(struct membro), 1);

    if(m == NULL)
        return NULL;

    fread(&m->tam_nome, sizeof(unsigned int), 1, arq);
    memset(m->nome, '\0', PATH_MAX);
    fread(m->nome, sizeof(char), m->tam_nome, arq);
    fread(&m->uid, sizeof(uid_t), 1, arq);
    fread(&m->grupo, sizeof(gid_t), 1, arq);
    fread(&m->permissoes, sizeof(mode_t), 1, arq);
    fread(&m->tamanho, sizeof(off_t), 1, arq);
    fread(&m->data_mod, sizeof(time_t), 1, arq);
    fread(&m->data_acc, sizeof(time_t), 1, arq);
    
    m->prox = NULL;

    return m;
}


// FUNÇÃO QUE VERIFICA SE OS NOMES SÃO IGUAIS
int comparaNomes(struct membro *m, char *membro)
{
    int a = 0;

    if(membro[0] == '/')
        a = 1;
    else if((membro[0] != '.') && (membro[1] != '/'))
        a = 2;

    if((m->tam_nome - a) != strlen(membro))
        return 0;

    for(int i = 0; i < m->tam_nome - a; ++i){
        if(m->nome[i + a] != membro[i])
            return 0;
    }   

    return 1;
}


// FUNÇÃO QUE BUSCA UM MEMBRO NA LISTA DE MEMBROS
struct membro *buscaMembroLista(struct lista *l, char *membro)
{
    struct membro *aux;
    int iguais;

    aux = l->ini;
    while(aux != NULL){
        iguais = comparaNomes(aux, membro);
        if(iguais)
            return aux;

        aux = aux->prox;
    }

    return NULL;
}


// FUNÇÃO QUE ATUALIZA A LISTA DE MEMBROS COMPLETAMENTE
void atualizaMembrosLista(struct lista *l)
{
    struct membro *aux;
    int ordem;
    long int endereco;

    ordem = 1;
    endereco = sizeof(int) + sizeof(long int);

    if(l == NULL || l->ini == NULL)
        return;

    aux = l->ini;
    
    while(aux != NULL){
        aux->ordem = ordem;
        aux->endereco = aux->endereco;

        ordem += 1;
        endereco += aux->tamanho;

        aux = aux->prox;
    }

    return;
}


// FUNÇÃO QUE SUBSTITUI OS DADOS DE UM MEMBRO NA LISTA DE MEMBROS
void substituiDadosMembro(struct lista *l, struct membro *ant, struct membro *novo)
{
    l->diretorios = l->diretorios - ant->tamanho + novo->tamanho;
 
    ant->uid = novo->uid;
    ant->grupo = novo->grupo;
    ant->permissoes = novo->permissoes;
    ant->tamanho = novo->tamanho;
    ant->data_mod = novo->data_mod;
    ant->data_acc = novo->data_acc;

    free(novo);
    atualizaMembrosLista(l);

    return;
}


// FUNÇÃO QUE CALCULA QUAL A PRÓXIMA POSIÇÃO PARA INSERÇÃO
void proximaPosicaoLista(struct lista *l, int *ordem, long int *endereco)
{
    if(l->ini == NULL)
        *ordem = 1;
    else
        *ordem = l->fim->ordem + 1;

    *endereco = l->diretorios;

    return;
}


// FUNÇÃO QUE INSERE UM NOVO MEMBRO NA LISTA DE MEMBROS
int insereMembroLista(struct lista *l, struct membro *m)
{
    int ordem;
    long int endereco;

    if(m == NULL)
        return 0;
    
    proximaPosicaoLista(l, &ordem, &endereco);

    if(l->ini == NULL){
        l->ini = m;
        l->fim = l->ini;
    }
    else{
        l->fim->prox = m;
        m->ant = l->fim;
        l->fim = l->fim->prox;
    }
    
    m->ordem = ordem;
    m->endereco = endereco;
    l->qtd += 1;
    l->diretorios = l->diretorios + m->tamanho;

    return 1;
}


// FUNÇÃO QUE MOVE UM MEMBRO NA LISTA DE MEMBROS
int moveMembroLista(struct lista *l, struct membro *a, struct membro *m)
{
    if(m == NULL || a == NULL || a->prox == m)
        return 0;

    if(m->ant == NULL)
        l->ini = m->prox;
    else
        m->ant->prox = m->prox;
    
    if(m->prox != NULL)
        m->prox->ant = m->ant;
    
    m->ant = a;
    m->prox = a->prox;
    a->prox = m;

    if(m->prox != NULL)
        m->prox->ant = m;

    atualizaMembrosLista(l);

    return 1;
}


// FUNÇÃO QUE REMOVE UM MEBRO DA LISTA DE MMEBROS
int removeMembroLista(struct lista *l, struct membro *m)
{
    if(m == NULL)
        return 0;

    if(m->ant == NULL)
        l->ini = m->prox;
    else
        m->ant->prox = m->prox;
    
    if(m->prox != NULL)
        m->prox->ant = m->ant;

    l->qtd -= 1;
    l->diretorios = l->diretorios - m->tamanho;
    atualizaMembrosLista(l);
    free(m);
    
    return 1;
}


// FUNÇÃO QUE IMPRIME A LISTA DE MEMBROS COM SEUS RESPECTIVOS DADOS
void imprimeMembrosLista(struct lista *l)
{
    struct membro *aux;
    struct tm data;
    struct passwd uid;
    struct group grupo;
    int ano, mes, dia, hora, min;

    if(l->ini == NULL){
        printf("Arquivo '%s' não possui nenhum membro.\n", l->arquivo);
        return;
    }

    aux = l->ini;

    while(aux != NULL){
        printf( (S_ISDIR(aux->permissoes)) ? "d" : "-");
        printf( (aux->permissoes & S_IRUSR) ? "r" : "-");
        printf( (aux->permissoes & S_IWUSR) ? "w" : "-");
        printf( (aux->permissoes & S_IXUSR) ? "x" : "-");
        printf( (aux->permissoes & S_IRGRP) ? "r" : "-");
        printf( (aux->permissoes & S_IWGRP) ? "w" : "-");
        printf( (aux->permissoes & S_IXGRP) ? "x" : "-");
        printf( (aux->permissoes & S_IROTH) ? "r" : "-");
        printf( (aux->permissoes & S_IWOTH) ? "w" : "-");
        printf( (aux->permissoes & S_IXOTH) ? "x" : "-");

        data = *localtime(&aux->data_mod);
        uid = *getpwuid(aux->uid);
        grupo = *getgrgid(aux->grupo);

        printf(" %s/%s ", uid.pw_name, grupo.gr_name);
        printf("%ld ", aux->tamanho);

        ano = data.tm_year+1900;
        mes = data.tm_mon+1;
        dia = data.tm_mday;
        hora = data.tm_hour;
        min = data.tm_min;
        printf("%d-", ano);
        
        if(mes < 10)
            printf("0%d-", mes);
        else
            printf("%d-", mes);

        if(dia < 10)
            printf("0%d ", dia);
        else
            printf("%d ", dia);
        
        if (hora < 10)
            printf("0%d:", hora);
        else
            printf("%d:", hora);

        if (min < 10)
            printf("0%d ", min);
        else
            printf("%d ", min);
    
        printf("%s\n", aux->nome);
        
        aux = aux->prox;
    }

    return;
}


// FUNÇÃO QUE CARREGA TODOS OS MEMBROS DE UM ARQUIVO VPP
struct lista *armazenaTodosMembros(FILE *arq, char *arquivo, int existe)
{
    struct lista *l;
    struct membro *m;
    int qtd, i = 0;
    long int diretorios;

    l = criaListaMembros(arquivo);

    if(l == NULL)
        return NULL;

    if(existe == 0)
        return l;

    rewind(arq);
    fread(&qtd, sizeof(int), 1, arq);
    fread(&diretorios, sizeof(long int), 1, arq);
    fseek(arq, diretorios, SEEK_SET);

    while(i < qtd){
        m = leDados(arq);
        insereMembroLista(l, m);
        ++i;
    }

    return l;
}


// FUNÇÃO QUE ESCREVE A LISTA DE MEMBROS NO ARQUIVO VPP
void gravaListaArquivo(struct lista *l, FILE *arq)
{
    struct membro *aux;

    rewind(arq);
    fwrite(&l->qtd, sizeof(int), 1, arq);
    fwrite(&l->diretorios, sizeof(long int), 1, arq);    
    fseek(arq, l->diretorios, SEEK_SET);
    aux = l->ini;

    while(aux != NULL){
        fwrite(&aux->tam_nome, sizeof(unsigned int), 1, arq);
        fwrite(&aux->nome, sizeof(char), aux->tam_nome, arq);
        fwrite(&aux->uid, sizeof(uid_t), 1, arq);
        fwrite(&aux->grupo, sizeof(gid_t), 1, arq);
        fwrite(&aux->permissoes, sizeof(mode_t), 1, arq);
        fwrite(&aux->tamanho, sizeof(off_t), 1, arq);
        fwrite(&aux->data_mod, sizeof(time_t), 1, arq);
        fwrite(&aux->data_acc, sizeof(time_t), 1, arq);

        aux = aux->prox;
    }

    return;
}
