
// BIBLIOTECAS UTILIZADAS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "vpp.h"

int getopt(int argc, char * const argv[], const char* optstring);
extern char* optarg;

// PROGRAMA PRINCIPAL
int main(int argc, char *argv[])
{
	short opt, s;
	int i = 0, existe = 1, a;
	int flag_i = 0, flag_a = 0, flag_m = 0, flag_x = 0, flag_r = 0, flag_c = 0, flag_h = 0;
	struct lista *l;
	FILE *arq;

	//PEGA AS OPÇÕES DE ENTRADA
	while((opt = getopt(argc, argv, "iam:xrch")) != -1){
		switch(opt) {
		case 'i':
			flag_i = 1;
			break;
		case 'a':
			flag_a = 1;
			break;
		case 'm':
			flag_m = 1;
			break;
		case 'x':
			flag_x = 1;
			break;
		case 'r':
			flag_r = 1;
			break;
		case 'c':
			flag_c = 1;
			break;
		case 'h':
			flag_h = 1;
			break;
		default:
			fprintf(stderr, "Erro: formatação de entrada incorreta!\n");
			printf("Para mais informações, execute o programa com -h\n");
			printf("\n      Comando: ./vina++ -h\n");
			exit(1);
		}
	}

	// TRATA O CASO DE SEREM PASSADAS MAIS DE UMA OPÇÃO
	s = flag_i + flag_a + flag_m + flag_x + flag_r + flag_c + flag_h;

	if(s > 1){
		fprintf(stderr, "Erro: mais de um argumento passado!\n");
		printf("Para mais informações, execute o programa com -h\n");
		printf("\n      Comando: ./vina++ -h\n");
		exit(1);
	}

	// TRATA O CASO DE NENHUMA OPÇÃO SER PASSADA
	else if(s == 0){
		fprintf(stderr, "Erro: nenhum argumento passado!\n");
		printf("Para mais informações, execute o programa com -h\n");
		printf("\n      Comando: ./vina++ -h\n");
		exit(1);
	}

	// TRATA O CASO DE NÃO SER PASSADA A QUANTIDADE MÍNIMA DE ARQUIVOS
	else if((flag_i || flag_a || flag_r) && (argc < 4)){
		fprintf(stderr, "Erro: quantidade de arquivos incorreta!\n");
		printf("Para mais informações, execute o programa com -h\n");
		printf("\n      Comando: ./vina++ -h\n");
		exit(1);	
	}
 
 	// TRATA O CASO DE NÃO SER PASSADA A QUANTIDADE CORRETA DE ARQUIVOS PARA O -M
	else if(flag_m && argc != 5){
		fprintf(stderr, "Erro: quantidade de arquivos incorreta!\n");
		printf("Para mais informações, execute o programa com -h\n");
		printf("\n      Comando: ./vina++ -h\n");
		exit(1);	
	}

	// TRATA O CASO DE NÃO SER PASSADA A QUANTIDADE MÍNIMA DE ARQUIVOS
	else if((flag_x || flag_c) && (argc < 3)){
		fprintf(stderr, "Erro: quantidade de arquivos incorreta!\n");
		printf("Para mais informações, execute o programa com -h\n");
		printf("\n      Comando: