
// BIBLIOTECAS UTILIZADAS
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>
#include <utime.h>
#include <fcntl.h>
#include <string.h>
#include "vpp.h"
#define TAM_BUFFER 1024


// FUNÇÃO QUE CARREGA OS DADOS DE UM MEMBRO A SER INSERIDO NO ARQUIVO VPP
struct membro *armazenaDados(char *membro)
{
    struct stat memb;
    struct membro *m;
    int a = 0;

    stat(membro, &memb);
    m = calloc(sizeof(struct membro), 1);

    if(m == NULL)
        return NULL;

    memset(m->nome, '\0', NAME_MAX);

    if(membro[0] == '/'){
    	m->nome[0] = '.';
    	a = 1;
    }

    else if((membro[0] != '.') && (membro[1] != '/')){
    	m->nome[0] = '.';
    	m->nome[1] = '/';
    	a = 2;
    }

    strcat(m->nome, membro);
    m->tam_nome = strlen(membro) + a;
    m->uid = memb.st_uid;
    m->grupo = memb.st_gid;
    m->permissoes = memb.st_mode;
    m->tamanho = memb.st_size;
    m->data_mod = memb.st_mtime;
    m->data_acc = memb.st_atime;

    return m;
}


// FUNÇÃO QUE CALCULA OS TAMANHO DO BUFFER PARA MANIPULAÇÕES
void calculaTamanhoBuffer(struct membro *m, int *div, int *mod)
{
	*div = m->tamanho / TAM_BUFFER;
	*mod = m->tamanho % TAM_BUFFER;
	return;
}


// FUNÇÃO QUE INSERE O CONTEÚDO DO NOVO ARQUIVO NO ARQUIVO VPP
void inserirMembroArquivo(FILE *arq, FILE *memb, struct membro *m)
{
	int div, mod;
	char *leitura;

	calculaTamanhoBuffer(m, &div, &mod);
	fseek(arq, m->endereco, SEEK_SET);

	if(div > 0){
		leitura = malloc(sizeof(char) * TAM_BUFFER + 1);

		if(leitura == NULL)
			return;

		for(int i = 0; i < div; ++i){
			memset(leitura, '\0', TAM_BUFFER + 1);
			fread(leitura, sizeof(char), TAM_BUFFER, memb);
			fwrite(leitura, sizeof(char), TAM_BUFFER, arq);
		}

		free(leitura);
	}

	if(mod > 0){
		leitura = calloc(sizeof(char), mod + 1);

		if(leitura == NULL)
			return;

		fread(leitura, sizeof(char), mod, memb);
		fwrite(leitura, sizeof(char), mod, arq);
	
		free(leitura);
	}

	return;
}


// FUNÇÃO QUE ESTENDE O ARQUIVO VPP
void estenderArquivo(struct lista *l, FILE *arq, struct membro *m, off_t diff)
{
	off_t tam;
	long int endereco;
	int div, mod, i = 0;
	char *leitura;

	truncate(l->arquivo, l->diretorios + diff);

	tam = l->diretorios - (m->endereco + m->tamanho);
	div = tam / TAM_BUFFER;
	mod = tam % TAM_BUFFER;

	if(div > 0){	
		fseek(arq, l->diretorios - TAM_BUFFER, SEEK_SET);
		leitura = malloc(sizeof(char) * TAM_BUFFER + 1);

		if(leitura == NULL)
			return;

		while(i < div){
			memset(leitura, '\0', TAM_BUFFER + 1);
			endereco = ftell(arq);
			fread(leitura, sizeof(char), TAM_BUFFER, arq);
			fseek(arq, endereco + diff, SEEK_SET);
			fwrite(leitura, sizeof(char), TAM_BUFFER, arq);
			fseek(arq, endereco - TAM_BUFFER, SEEK_SET);
			++i;
		}
	
		free(leitura);
	}

	if(mod > 0){
		fseek(arq, m->endereco + m->tamanho, SEEK_SET);

		leitura = calloc(sizeof(char), mod + 1);

		if(leitura == NULL)
			return;

		fread(leitura, sizeof(char), mod, arq);
		fseek(arq, m->endereco + m->tamanho + diff, SEEK_SET);
		fwrite(leitura, sizeof(char), mod, arq);

		free(leitura);
	}

	return;
}


// FUNÇÃO QUE ENCURTA O ARQUIVO VPP
void encurtarArquivo(struct lista *l, FILE *arq, struct membro *m, off_t diff)
{
	off_t tam;
	long int endereco;
	int div, mod, i = 0;
	char *leitura;

	tam = l->diretorios - (m->endereco + m->tamanho);
	div = tam / TAM_BUFFER;
	mod = tam % TAM_BUFFER;

	if(diff > 0)
		diff = - diff;

	fseek(arq, m->endereco + m->tamanho, SEEK_SET);
	
	if(div > 0){
		
		leitura = malloc(sizeof(char) * TAM_BUFFER + 1);

		if(leitura == NULL)
			return;

		while(i < div){
			memset(leitura, '\0', TAM_BUFFER + 1);
			endereco = ftell(arq);
			fread(leitura, sizeof(char), TAM_BUFFER, arq);
			fseek(arq, endereco + diff, SEEK_SET);
			fwrite(leitura, sizeof(char), TAM_BUFFER, arq);
			fseek(arq, endereco + TAM_BUFFER, SEEK_SET);
			++i;
		}

		free(leitura);
	}

	if(mod > 0){
		leitura = calloc(sizeof(char), mod + 1);

		if(leitura == NULL)
			return;

		fseek(arq, m->endereco + m->tamanho + div * TAM_BUFFER, SEEK_SET);
		endereco = ftell(arq);
		fread(leitura, sizeof(char), mod, arq);
		fseek(arq, endereco + diff, SEEK_SET);
		fwrite(leitura, sizeof(char), mod, arq);
	
		free(leitura);
	}

	truncate(l->arquivo, l->diretorios + diff);

	return;
}


// FUNÇÃO QUE VERIFICA SE O TAMANHO DO ARQUIVO VPP PRECISA SER MANIPULADO
void ajustaTamanhoArquivo(struct lista *l, FILE *arq, FILE *memb, struct membro *m, off_t diff)
{
	if(diff > 0)
		estenderArquivo(l, arq, m, diff);

	else if(diff < 0)
		encurtarArquivo(l, arq, m, diff);

	return;
}


// FUNÇÃO QUE VERIFICA AS POSSIBILIDADES DE INSERÇÃO
void inserirMembro(int flag_a, struct lista *l, FILE *arq, char *membro)
{
	FILE *memb;
	struct membro *ant, *novo;
	off_t diff;

	rewind(arq);
	fwrite(&l->qtd, sizeof(int), 1, arq);
	fwrite(&l->diretorios, sizeof(long int), 1, arq);

	memb = fopen(membro, "r");

	if(memb == NULL){
		perror("");
		fprintf(stderr, "Erro ao abrir o arquivo '%s'\n", membro);
		return;
	}

	novo = armazenaDados(membro);
	ant = buscaMembroLista(l, membro);

	if(ant != NULL){
		if(flag_a && (ant->data_mod <= novo->data_mod)){
			fclose(memb);
			return;
		}

		diff = novo->tamanho - ant->tamanho;
		ajustaTamanhoArquivo(l, arq, memb, ant, diff);
		substituiDadosMembro(l, ant, novo);
		inserirMembroArquivo(arq, memb, ant);
	}
	
	else{
		insereMembroLista(l, novo);
		inserirMembroArquivo(arq, memb, novo);
	}
	

	fclose(memb);

	return;
}


// FUNÇÃO QUE MOVE A POSIÇÃO DE UM MEMBRO NO ARQUIVO VPP
void moverArquivoMembro(struct lista *l, FILE *arq, struct membro *a, struct membro *m)
{
	off_t diff;
	long int endereco;
	int div, mod, i;
	char *leitura;
	
	if(a->prox == NULL)
		diff = l->diretorios - m->endereco;	
	else if(a->prox->endereco < m->endereco)
		diff = a->prox->endereco - (m->endereco + m->tamanho);
	else
		diff = a->prox->endereco - m->endereco;
		
	div = m->tamanho / TAM_BUFFER;
	mod = m->tamanho % TAM_BUFFER;

	if(div > 0){
		fseek(arq, m->endereco, SEEK_SET);
		leitura = malloc(sizeof(char) * TAM_BUFFER + 1);

		if(leitura == NULL)
			return;

		i = 0;
		while(i < div){
			memset(leitura, '\0', TAM_BUFFER + 1);
			endereco = ftell(arq);
			fread(leitura, sizeof(char), TAM_BUFFER, arq);
			fseek(arq, endereco + diff, SEEK_SET);
			fwrite(leitura, sizeof(char), TAM_BUFFER, arq);
			fseek(arq, endereco + TAM_BUFFER, SEEK_SET);
			++i;
		}
	
		free(leitura);
	}

	if(mod > 0){
		fseek(arq, m->endereco + div * TAM_BUFFER, SEEK_SET);

		leitura = calloc(sizeof(char), mod + 1);

		if(leitura == NULL)
			return;

		endereco = ftell(arq);
		fread(leitura, sizeof(char), mod, arq);
		fseek(arq, endereco + diff, SEEK_SET);
		fwrite(leitura, sizeof(char), mod, arq);
		
		free(leitura);
	}

	return;
}


// FUNÇÃO QUE VERIFICA AS POSSIBILIDADES DE MOVIMENTAÇÃO DE UM MEMBRO NO ARQUIVO VPP
void moverMembro(struct lista *l, FILE *arq, char *alvo, char *membro)
{
	struct membro *a, *m;

	a = buscaMembroLista(l, alvo);
	m = buscaMembroLista(l, membro);

	if(l->ini == NULL){
		printf("Arquivo '%s' não possui nenhum membro.\n", l->arquivo);
		return;
	}

	if(a == NULL){
		printf("Arquivo '%s' não encontrado.\n", alvo);
		return;
	}

	if(m == NULL){
		printf("Arquivo '%s' não encontrado.\n", membro);
		return;
	}

	if(a->prox == m || a == m){
		printf("Arquivo já está na posição indicada.\n");
		return;
	}

	if(a->prox != NULL)
		estenderArquivo(l, arq, a, m->tamanho);
	
	moverArquivoMembro(l, arq, a, m);
	
	if((a->prox != NULL) && (a->prox->endereco < m->endereco))
		m->endereco = m->endereco + m->tamanho;

	l->diretorios = l->diretorios + m->tamanho;
	encurtarArquivo(l, arq, m, m->tamanho);

	moveMembroLista(l, a, m);

	return;
}


// FUNÇÃO QUE CRIA A ÁRVORE DE DIRETÓRIOS DESCRITA NO NOME DO MEMBRO
void criaArvoreDiretorios(char *nome, int i, int j)
{
	char caractere, diretorio[NAME_MAX];

	memset(diretorio, '\0', NAME_MAX);
	caractere = nome[i];
	
	while(caractere != '/'){
		diretorio[j] = caractere;
		++j;
		++i;
		if(i >= strlen(nome))
			break;
		caractere = nome[i];
	}

	if(i >= strlen(nome))
		return;

	mkdir(diretorio, S_IRWXU | (S_IRGRP | S_IXGRP) | (S_IROTH | S_IWOTH));
	chdir(diretorio);

	criaArvoreDiretorios(nome, i + 1, 0);

	return;
}


// FUNÇÃO QUE MODIFICA OS DADOS DO ARQUIVO EXTRAÍDO
void modificarDadosArquivos(struct membro *m)
{
	struct utimbuf *datas;

	datas = malloc(sizeof(struct utimbuf));
	datas->actime = m->data_acc;
	datas->modtime = m->data_mod;

	chmod(m->nome, m->permissoes);
	chown(m->nome, m->uid, m->grupo);
	utime(m->nome, datas);

	free(datas);

	return;
}


// FUNÇÃO QUE ENTRAI UM ÚNICO MEMBRO DO ARQUIVO VPP
void extraiUnicoMembro(struct lista *l, FILE *arq, struct membro *m)
{
	char *leitura, diretorio_vpp[PATH_MAX];
	int div, mod;
	FILE *novo;

	if(getcwd(diretorio_vpp, PATH_MAX) == NULL)
		return;

	criaArvoreDiretorios(m->nome, 0, 0);
	chdir(diretorio_vpp);	

	creat(m->nome, m->permissoes);
	novo = fopen(m->nome, "w");

	if(novo == NULL)
		return;

	calculaTamanhoBuffer(m, &div, &mod);
	fseek(arq, m->endereco, SEEK_SET);
	
	if(div > 0){
		leitura = malloc(sizeof(char) * TAM_BUFFER + 1);

		if(leitura == NULL)
			return;

		for(int i = 0; i < div; ++i){
			memset(leitura, '\0', TAM_BUFFER + 1);
			fread(leitura, sizeof(char), TAM_BUFFER, arq);
			fwrite(leitura, sizeof(char), TAM_BUFFER, novo);
		}

		free(leitura);
	}

	if(mod > 0){
		leitura = calloc(sizeof(char), mod + 1);

		if(leitura == NULL)
			return;

		fread(leitura, sizeof(char), mod, arq);
		fwrite(leitura, sizeof(char), mod, novo);
	
		free(leitura);
	}

	fclose(novo);
	modificarDadosArquivos(m);

	return;
}


// FUNÇÃO QUE VERIFICA AS POSSIBILIDADES DE EXTRAÇÃO DE UM MEMBRO
void extraiMembro(struct lista *l, FILE *arq, char *membro)
{
	struct membro *m;

	m = buscaMembroLista(l, membro);

	if(l->ini == NULL){
		printf("Arquivo '%s' não possui nenhum membro.\n", l->arquivo);
		return;
	}

	if(m == NULL){
		printf("Arquivo '%s' não encontrado.\n", membro);
		return;
	}

	extraiUnicoMembro(l, arq, m);

	return;
}


// FUNÇÃO QUE ESTRAI TODOS OS MEMBROS DO ARQUIVO VPP
void extraiTodosMembros(struct lista *l, FILE *arq)
{
	struct membro *aux;

	if(l->ini == NULL){
		printf("Arquivo '%s' não possui nenhum membro.\n", l->arquivo);
		return;
	}

	aux = l->ini;
	while(aux != NULL){
		extraiUnicoMembro(l, arq, aux);
		aux = aux->prox;
	}

	return;
}


// FUNÇÃO QUE REMOVE UM MEMBRO DO ARQUIVO VPP
void removerMembro(struct lista *l, FILE *arq, char *membro)
{
	struct membro *m;

	m = buscaMembroLista(l, membro);

	if(l->ini == NULL){
		printf("Arquivo '%s' não possui nenhum membro.\n", l->arquivo);
		return;
	}
	
	if(m == NULL){
		printf("Arquivo '%s' não encontrado.\n", membro);
		return;
	}

	encurtarArquivo(l, arq, m, -(m->tamanho));
	removeMembroLista(l, m);
}

// FUNÇÃO QUE IMPRIME UMA MENSAGEM COM INSTRUÇÕES SOBRE O PROGRAMA
void imprimeInstrucoes()
{
	printf("\n===== GUIA DE UTILIZAÇÃO DO VINA++ =====\n");

	printf("\n  - INSERÇÃO:\n");
	printf("    -i : Insere/acrescenta um ou mais membros ao arquivo_vpp.\n");
	printf("      Caso o membro já exista no arquivo_vpp, ele deve ser substituído.\n");
	printf("      Novos membros são inseridos respeitando a ordem da linha de comando, ao final do arquivo_vpp.\n");
	printf("      Comando: ./vina++ -i <arquivo_vpp> <membro_1> <membro_2> ... <membro_n>\n");

	printf("\n  - INSERÇÃO COM VERIFICAÇÃO:\n");
	printf("    -a : Mesmo comportamento da opção -i.\n");
	printf("      Porém a substituição de um membro existente ocorre APENAS caso o parâmetro seja mais recente que o arquivado.\n");
	printf("      Comando: ./vina++ -a <arquivo_vpp> <membro_1> <membro_2> ... <membro_n>\n");

	printf("\n  - MOVIMENTAÇÃO:\n");
	printf("    -m : Move o membro indicado na linha de comando para imediatamente depois do membro alvo existente no arquivo_vpp.\n");
	printf("      Comando: ./vina++ -m <alvo> <arquivo_vpp>  <membro>\n");

	printf("\n  - EXTRAÇÃO:\n");
	printf("    -x : Extrai os membros indicados de arquivo_vpp.\n");
	printf("      Se os membros não forem indicados, todos serão extraídos.\n");
	printf("      Comando: ./vina++ -x <arquivo_vpp>  <membro_1> <membro_2> ... <membro_n>\n");

	printf("\n  - REMOÇÃO:\n");
	printf("    -r : Remove os membros indicados do arquivo_vpp.\n");
	printf("      Comando: ./vina++ -r <arquivo_vpp>  <membro_1> <membro_2> ... <membro_n>\n");

	printf("\n  - IMPRESSÃO:\n");
	printf("    -c : Lista o conteúdo do arquivo_vpp em ordem.\n");
	printf("      Imprime as propriedades de cada membro (nome, UID, permissões, tamanho e data de modificação).\n");
	printf("      Comando: ./vina++ -c <arquivo_vpp>\n");

	printf("\n  - INSTRUÇÕES:\n");
	printf("    -h : Lista as instruções de uso do vina++.\n\n");

	return;
}
