
// BIBLIOTECAS UTILIZADAS
#include <unistd.h>
#include <limits.h>


// ESTRUTURA PARA ARMAZENAR OS DADOS DE UM MEMBRO
struct membro{
    unsigned int tam_nome;
    char nome[PATH_MAX];
    uid_t uid;
    gid_t grupo;
    mode_t permissoes;
    off_t tamanho;
    time_t data_mod;
    time_t data_acc;
    int ordem;
    long int endereco;
    struct membro *prox;
    struct membro *ant;
};


// ESTRUTURA DE CABEÇALHO DA LISTA DE MEMBROS
struct lista{
    char arquivo[NAME_MAX];
    struct membro *ini;
    struct membro *fim;
    int qtd;
    long int diretorios;
};


////////// FUNÇÕES QUE TRATAM A LISTA DE MEMBROS  //////////

// FUNÇÃO QUE DESTRÓI A LISTA DE MEMBROS
struct lista *destroiListaMembros(struct lista *l);

// FUNÇÃO QUE BUSCA UM MEMBRO NA LISTA DE MEMBROS
struct membro *buscaMembroLista(struct lista *l, char *membro);

// FUNÇÃO QUE SUBSTITUI OS DADOS DE UM MEMBRO
void substituiDadosMembro(struct lista *l, struct membro *ant, struct membro *novo);

// FUNÇÃO QUE INSERE UM NOVO MEMBRO NA LISTA
int insereMembroLista(struct lista *l, struct membro *m);

// FUNÇÃO QUE MOVE A POSIÇÃO DE UM MEMBRO NA LISTA
int moveMembroLista(struct lista *l, struct membro *a, struct membro *m);

// FUNÇÃO QUE REMOVE UM MEMBRO DA LISTA
int removeMembroLista(struct lista *l, struct membro *m);

// FUNÇÃO QUE IMPRIME TODOS OS MEMBROS DA LISTA
void imprimeMembrosLista(struct lista *l);

// FUNÇÃO QUE CARREGA TODOS OS MEMBROS EXISTENTES DE UMAARQUIVO VPP
struct lista *armazenaTodosMembros(FILE *arq, char *arquivo, int existe);

// FUNÇÃO QUE ESCREVE TODA A LISTA DE MEMBROS NO ARQUIVO VPP
void gravaListaArquivo(struct lista *l, FILE *arq);


////////// FUNÇÕES QUE TRATAM DAS MANIPULAÇÕES NO ARQUIVO VPP  //////////

// FUNÇÃO QUE INSERE UM NOVO MEMBRO NO ARQUIVO VPP
void inserirMembro(int flag_a, struct lista *l, FILE *arq, char *membro);

// FUNÇÃO QUE MOVE UM MEBRO NO ARQUIVO VPP
void moverMembro(struct lista *l, FILE *arq, char *alvo, char *membro);

// FUNÇÃO QUE EXTRAI UM MEMBRO DO ARQUIVO VPP
void extraiMembro(struct lista *l, FILE *arq, char *membro);

// FUNÇÃO QUE EXTRAI TODOS OS MEMBROS DO ARQUIVO VPP
void extraiTodosMembros(struct lista *l, FILE *arq);

// FUNÇÃO QUE REMOVE UM ARQUIVO DO ARQUIVO VPP
void removerMembro(struct lista *l, FILE *arq, char *membro);

// FUNÇÃO QUE IMPRIME UMA MENSAGEM COM AS INTRUÇÕES SOBRE O PROGRAMA
void imprimeInstrucoes();
