#include <stdio.h>
#include <stdlib.h>

//Estrutura elemento
elem_t {

}


//Estrutura fila
struct fila_t {
	elem_t **v;
	int lim;
	int fim;
}


//Função para criar a fila
struct fila_t criaFila(unsigned int lim)
{
	struct fila_t *f;

	f = malloc(sizeof(struct fila_t)*);

	if(f == NULL)
		return NULL;

	f->v = malloc(sizeof(struct elem_t) * lim);

	if(f->v == NULL){
		free(f);
		return NULL;
	}

	f->lim = lim;
	f->fim = 0;

	return f;
}


//Função que realiza a inserção na fila
int insere(elem_t *elem, fila_t *f)
{
	if(ehCheia(f))
		return 0;
	f->fim += 1;

	return 1;
}


//Verifica se a fila vazia
int ehVazia(struct fila_t *f)
{
	if(f->fim == 0)
		return 1;
	return 0;
}
