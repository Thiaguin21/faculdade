#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#define SIZE 100

int main(){
	char entrada[SIZE];

	fgets(entrada, SIZE, stdin);

	entrada[strcspn (entrada, "\n")] = '\0';

	for(int i=strlen(entrada); i >= 0; i--){
		putchar(entrada[i]);
	}

	printf("\n");

	return 0;
}
