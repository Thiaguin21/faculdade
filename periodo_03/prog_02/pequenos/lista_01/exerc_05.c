#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#define SIZE 100


int main(){
	char entrada[SIZE];

	fgets(entrada, SIZE, stdin);

	entrada[strcspn (entrada, "\n")] = '\0';	

	for(int i = 0; i < strlen(entrada); i++){
		if(entrada[i] >= 65 && entrada[i] <= 90)
			entrada[i] += 32; 
	}

	printf("%s\n", entrada);
	
	return 0;
}