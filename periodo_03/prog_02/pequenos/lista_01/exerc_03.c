#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <ctype.h>
#define SIZE 100


int main(){
	char entrada[SIZE];

	fgets(entrada, SIZE, stdin);

	entrada[strcspn (entrada, "\n")] = '\0';

	for(int i = 0; i < strlen(entrada); i++){
		entrada[i] = tolower(entrada[i]);
	}

	printf("%s\n", entrada);

	return 0;
}
