#include <stdio.h>
#include <stdlib.h>
#define SIZE 1024

int main(){
	FILE* arq;
	char entrada[SIZE+1];
	float i = 0, soma = 0;

	arq = fopen("teste.txt", "r");

	if(!arq){
    	ferror(arq);
    	exit(1); // encerra o programa com status 1
   	}
	 
	fgets(entrada, SIZE, arq); 
	while(!feof(arq)){
		soma = soma + atof(entrada);
		fgets(entrada, SIZE, arq);
		i++;
	}

	printf("%f\n", soma/i);

	fclose(arq);

	return 0;
}
