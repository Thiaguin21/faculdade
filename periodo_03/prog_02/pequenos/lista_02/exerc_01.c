#include <stdio.h>
#include <stdlib.h>

int main(){
	FILE* arq;
	arq = fopen("teste.txt", "r");
	int i = 0;

	if(!arq){
    	perror ("Erro ao abrir arquivo x");
    	exit(1); // encerra o programa com status 1
   	}

	while(!feof(arq)){
		fgetc(arq);
		i++;
	}

	printf("%d\n", i);

	return 0;
}
