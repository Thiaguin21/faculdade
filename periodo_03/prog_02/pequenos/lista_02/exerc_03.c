#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#define SIZE 10

int main(){
	FILE* entrada;
	FILE* saida;
	char leitura[SIZE];

	entrada = fopen("minusc.txt", "r");

	if(!entrada){
    	ferror(entrada);
    	exit(1); // encerra o programa com status 1
   	}

	saida = fopen("maiusc.txt", "a");

	if(!saida){
    	ferror(saida);
    	exit(1); // encerra o programa com status 1
   	}

	fscanf(entrada, "%s", leitura);
	while(!feof(entrada)){
		for(int i = 0; i < SIZE; ++i){
			if(islower(leitura[i]))
				leitura[i] = toupper(leitura[i]);
		}
		fputs(leitura, saida);
		fscanf(entrada, "%s", leitura);
	}

	fclose(entrada);
	fclose(saida);

	return 0;
}
