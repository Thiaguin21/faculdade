#include <stdio.h>
#include <stdlib.h>
#define MIN 0
#define MAX 1000


int compara(int *x, int *y)
{
	if (*x > *y)
		return 1;
	else if (*x == *y)
		return 0;
	return -1;
}



int main(){
	srand(0);
	int v[100];
	int num_rand;
//	int (*ptr) (void *, void *);
//	ptr = compara;

	for(int i=0; i<100; ++i){
		num_rand = rand() % (MAX - MIN + 1) + MIN;
		v[i] = num_rand;
	}

	for(int i=0; i<100; ++i)
		printf("%d ", v[i]);
	printf("\n");

	qsort(&v, 100, sizeof(int), (void*) compara);

	for(int i=0; i<100; ++i)
		printf("%d ", v[i]);
	printf("\n");

	return 0;
}

