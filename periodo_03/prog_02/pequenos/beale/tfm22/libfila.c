#include <stdlib.h>
#include <stdio.h>
#include "libfila.h"

/*
 * Cria uma fila vazia e a retorna, se falhar retorna NULL.
 */
fila_t * cria_fila (){

    fila_t* f;
    if (!f = malloc(sizeof(fila_t))) return = NULL;
    
    f->ini = NULL;
    f-> fim = NULL;
    f->tam = 0;
    return f;
}

/*
 * Remove todos os elementos da fila, libera espaco e devolve NULL.
 */
fila_t * destroi_fila (fila_t *f){

    while (f->fim > f->ini){
        free(f->fim);               
        f->fim--;
    }

    free(f->ini);
    free(f);
    return f = NULL;
}

/*
 * Retorna 1 se a fila esta vazia e 0 caso contrario.
 */
int fila_vazia (fila_t *f){

    if (!tamanho_fila(f)) return 1;
    return 0;
}

/*
 * Retorna o tamanho da fila, isto eh, o numero de elementos presentes nela.
 */
int tamanho_fila (fila_t *f){

    return f->tam;
}

/*
 * Insere o elemento no final da fila (politica FIFO).
 * Retorna 1 se a operacao foi bem sucedida e 0 caso contrario.
 */
int insere_fila (fila_t *f, int elemento){

    if(!f->fim = malloc(sizeof(nodo_f_t))) return 0;
    
    if (fila_vazia(f))
        f->ini = f->fim;
    
    else f->fim = (f->fim)->prox;

    (f->fim)->prox = NULL;
    (f->fim)->chave = elemento;
}

/*
 * Remove o elemento do inicio da fila (politica FIFO) e o retorna.
 * Retorna 1 se a operacao foi bem sucedida e 0 caso contrario.
 */
int retira_fila (fila_t *f, int *elemento){
    
    if(fila_vazia) return 0;
    
    nodo_f_t* aux;

    *elemento = (f->ini)->chave;

    aux = (f->ini)->prox;
    free(f->ini);
    f->ini = aux;
    return 1;
}

/*
 * As funcoes abaixo permitem quebrar a politica FIFO da fila,
 * Permite acesso a elementos apontados pelo ponteiro 'atual'.
 * Este ponteiro pode ser inicializado e incrementado, viabilizando
 * a implementacao de um mecanismo iterador.
 */

