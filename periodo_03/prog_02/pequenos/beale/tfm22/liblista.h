
typedef  struct caracter {
    char elem;
    int* chaves;  
    int tam;
} caracter_t;


typedef struct nodo {
    caracter_t *caracter;
    struct nodo *prox;
} nodo_t;


typedef struct lista {
    nodo_t *primeiro;
} lista_t;


lista_t *cria_lista();

nodo_t *destroi_nodo(nodo_t *n);

lista_t *destroi_lista(lista_t *l);

int adiciona_ordem_lista(lista_t *l, char c, int chave);

char *caracter_e_chaves(nodo_t *nodo);
