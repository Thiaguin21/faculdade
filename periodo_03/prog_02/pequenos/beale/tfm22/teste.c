#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#define SIZE 100

struct nodo_chave {
    unsigned long chave;
    struct nodo_chave* prox;
};

struct lista_chaves {
    struct nodo_chave* ini;
    struct nodo_chave* fim;
    int tam;
};

struct nodo_caracter {
    char caracter;
    struct lista_chaves* chaves;
    struct nodo_caracter* prox;
};

struct lista_caracteres {
    struct nodo_caracter* ini;
    struct nodo_caracter* fim;
};


struct lista_caracteres* cria_lista_caracteres(){

    struct lista_caracteres* l;
    if(!(l = malloc(sizeof(struct lista_caracteres))))
        return 0;

    l->ini = NULL;
    l->fim = l->ini;

    return l;
}


struct lista_chaves* cria_lista_chaves(){

    struct lista_chaves* l;
    if(!(l = malloc(sizeof(struct lista_chaves))))
        return 0;

    l->ini = NULL;
    l->fim = NULL;
    l->tam = 0;

    return l;
}


struct lista_chaves* destroi_lista_chaves(struct lista_chaves* l){
    struct nodo_chave* aux1;

    while(l->ini != l->fim){
        aux1 = l->ini;
        l->ini = aux1->prox;
        free(aux1);
    }
    free(l->fim);
    free(l);

    return NULL;
}


struct lista_caracteres* destroi_lista_caracteres(struct lista_caracteres* l){
    struct nodo_caracter* aux1;

    if(l->ini != NULL){
        while(l->ini != l->fim){
            aux1 = l->ini;
            l->ini = aux1->prox;
            destroi_lista_chaves(aux1->chaves);
            free(aux1);
        }
        free(l->fim);
    }
    free(l);

    return NULL;
}


int insere_lista_chaves(struct lista_chaves* l, unsigned long int chave){
    struct nodo_chave* n;

    if(!(n = malloc(sizeof(struct nodo_chave))))
        return 0;

    n->chave = chave;
    n->prox = NULL;
    l->tam += 1;

    if(l->ini == NULL){
        l->ini = n;
        l->fim = l->ini;
        return 1;
    }

    (l->fim)->prox = n;
    l->fim = (l->fim)->prox;

    return 1;
}


struct nodo_caracter* cria_nodo_caracter(char c, unsigned long int num, struct nodo_caracter* prox){
    struct nodo_caracter* n;

    if(!(n = malloc(sizeof(struct nodo_caracter))))
        return NULL;

    n->caracter = tolower(c);
    n->chaves = cria_lista_chaves();
    n->prox = prox;
    insere_lista_chaves(n->chaves, num);

    return n;
}


int insere_lista_caracteres(struct lista_caracteres* l, char c, unsigned long int num){
    struct nodo_caracter* aux1;
    struct nodo_caracter* aux2;

    //LISTA VAZIA
    if(l->ini == NULL){
        l->ini = cria_nodo_caracter(c,num,NULL);
        return 1;
    }

    aux1 = l->ini;
    if(tolower(aux1->caracter) > tolower(c)){
        l->ini = cria_nodo_caracter(c,num,aux1);
        return 1;
    }

    else if(tolower(aux1->caracter) == tolower(c)){
        insere_lista_chaves(aux1->chaves, num);
        return 1;
    }

    aux2 = aux1->prox;
    if(aux1->prox == NULL){
        aux1->prox = cria_nodo_caracter(c,num,NULL);
        return 1;
    }

    while(aux2->prox != NULL){

        if(tolower(aux2->caracter) > tolower(c)){
            aux1->prox = cria_nodo_caracter(c,num,aux2);
            return 1;
        }

        if(tolower(aux2->caracter) == tolower(c)){
            insere_lista_chaves(aux2->chaves, num);
            return 1;
        }

        aux1 = aux2;
        aux2 = aux2->prox;
    }

    if(tolower(aux2->caracter) == tolower(c)){
            insere_lista_chaves(aux2->chaves, num);
            return 1;
    }

    aux2->prox = cria_nodo_caracter(c,num,NULL);

    return 1;
}


void gera_lista_chaves(FILE* livro_cifras, struct lista_caracteres* l){
    unsigned long int i = 0;
    char leitura[SIZE];

    fscanf(livro_cifras, "%s", leitura);
    while(!feof(livro_cifras)){
        for (int j = 0; j < strlen(leitura); ++j){
            if(isprint(leitura[j])){
                insere_lista_caracteres(l, leitura[j], i);
                break;
            }
        }

        ++i;
        fscanf(livro_cifras, "%s", leitura);
    }

    return;
}

void gera_arquivo_chaves(FILE* arquivo_chaves, struct lista_caracteres* l){
        struct nodo_caracter* aux1;
        struct nodo_chave* aux2;
        struct lista_chaves* chaves;
        aux1 = l->ini;

        while(aux1 != NULL){
            fprintf(arquivo_chaves, "%c: ",aux1->caracter);
            chaves = aux1->chaves;
            aux2 = chaves->ini;
            while(aux2 != chaves->fim){
                fprintf(arquivo_chaves, "%ld ",aux2->chave);
                aux2 = aux2->prox;
            }
            fprintf(arquivo_chaves, "%ld\n",(chaves->fim)->chave);
            aux1 = aux1->prox;
        }

        return;
}


//struct nodo_caracter* busca_caractere(struct lista_caracteres* l,char c){
//    struct nodo_caracter* aux;
//    aux = l->ini;
//
//    if(tolower((l->fim->)caracter) < tolower(c))
//        return NULL;
//
//    else if((l->fim->)caracter == tolower(c))
//        return l->fim;
//
//    while(aux != l->fim){
//        if(aux->caracter == tolower(c))
//            return aux;
//        aux = aux->prox
//    }
//
//    return NULL;
//}


//int codifica(FILE* mensagem_original){
//    FILE* arquivo_codificado;
//    struct nodo_caracter* n;
//    char leitura[SIZE];
//    unsigned long int chave;
//
//    arquivo_codificado = fopen("ArquivoCodificado.txt","r");
//
//    if(!arquivo_codificado){
//        perror("Erro ao abrir o arquivo ArquivoCodificado.txt");
//        exit(1);
//    }
//
//    fscanf(mensagem_original, "%s", leitura);
//    while(!feof(mensagem_original)){
//        for (int i = 0; i < strlen(leitura); ++i){
//            n = busca_caractere(leitura[i]);
//            if(n == NULL)
//                fprintf(arquivo_codificado, "?");
//            else{
//                chave = gera_chave_randomica(n->chaves);
//                fprintf(arquivo_codificado, "?");
//            }
//        }
//        fscanf(mensagem_original, "%s", leitura);
//    }
//
//    fclose(arquivo_codificado);
//
//    return 1;
//}

int main(){
    FILE* livro_cifras;
    FILE* arquivo_chaves;
    struct lista_caracteres* l;

    livro_cifras = fopen("LivroCifra.txt","r");

    if(!livro_cifras){
        perror("Erro ao abrir o arquivo LivroCifra.txt");
        exit(1);
    }

    arquivo_chaves = fopen("ArquivoChaves.txt", "w");

    if(!arquivo_chaves){
        perror("Erro ao abrir/criar o arquivo ArquivoChaves.txt");
        exit(1);
    }

    l = cria_lista_caracteres();
    gera_lista_chaves(livro_cifras, l);
    gera_arquivo_chaves(arquivo_chaves, l);

    l = destroi_lista_caracteres(l);
    fclose(livro_cifras);
    fclose(arquivo_chaves);

    return 0;
}
