#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include "liblista.h"
#define SIZE 100


lista_t *cria_lista (){
    
    lista_t *l;
    if(!(l = malloc(sizeof(lista_t))))
        return 0;
    
    l->primeiro = NULL;
   
    return l;
}


nodo_t *destroi_nodo(nodo_t *n){

    free((n->caracter)->chaves);
    free(n->caracter);
    n->prox = NULL;
    free(n);

    return NULL;
}


lista_t *destroi_lista (lista_t *l){
    
    nodo_t* aux;
    while(l->primeiro != NULL){
        aux = l->primeiro;
        l->primeiro = aux->prox;
        destroi_nodo(aux);
    }
    
    free(l);
    
    return NULL;
}


int adiciona_ordem_lista (lista_t *l, char c, int chave){
    nodo_t *aux1, *aux2;
    nodo_t *novo_nodo;
    caracter_t *novo_caracter, *existente;
    
    if(!(novo_nodo = malloc(sizeof(nodo_t))))
        return 0;

    if(!(novo_caracter = malloc(sizeof(caracter_t))))
        return 0;

    if(!(novo_caracter->chaves = malloc(sizeof(int)*1)))
        return 0;

    novo_caracter->chaves[0] = chave;
    novo_caracter->elem = c;
    novo_caracter->tam = 1;

    novo_nodo->caracter = novo_caracter;
    novo_nodo->prox = NULL;

    //A LISTA ESTÁ VAZIA
    if(l->primeiro == NULL){
        l->primeiro = novo_nodo;
        return 1;
    }

    //A LISTA TEM PELO MENOS 1 ELEMENTO
    else{
        aux1 = l->primeiro;
        if((aux1->caracter)->elem > novo_caracter->elem){
            l->primeiro = novo_nodo;
            novo_nodo->prox = aux1;
            return 1;
        }
     
        else if(toupper((aux1->caracter)->elem) == novo_caracter->elem || tolower((aux1->caracter)->elem) == novo_caracter->elem){
            destroi_nodo(novo_nodo);
            existente = aux1->caracter;
            existente->tam += 1;
            if(!(existente->chaves = realloc(existente->chaves,sizeof(int)*existente->tam)))
                return 0;
            existente->chaves[existente->tam-1] = chave;
            return 1;
        }
    }
    
    //A LISTA TEM UM ÚNICO ELEMENTO
    aux2 = aux1->prox;
    if(aux2 == NULL){
        aux1->prox = novo_nodo;
        return 1;
    }

    //A LISTA TEM MAIS DE UM ELEMENTO
    while(aux2 != NULL){
        
        if((aux2->caracter)->elem > novo_caracter->elem){
            aux1->prox = novo_nodo;
            novo_nodo->prox = aux2;
            return 1;
        }

        else if(toupper((aux1->caracter)->elem) == novo_caracter->elem || tolower((aux1->caracter)->elem) == novo_caracter->elem){
            destroi_nodo(novo_nodo);
            existente = aux1->caracter;
            existente->tam += 1;
            if(!(existente->chaves = realloc(existente->chaves,sizeof(int)*existente->tam)))
                return 0;
            existente->chaves[existente->tam -1] = chave;
            return 1;
        }

        aux1 = aux2;
        aux2 = aux2->prox;
    }

    aux1->prox = novo_nodo;
    
    return 1;
}


char* caracter_e_chaves(nodo_t *nodo){
    caracter_t *c;
    char elem[SIZE];
    char vetor[SIZE];
    char* string;
    string = malloc(sizeof(char)*SIZE);
    c = nodo->caracter;

    sprintf(vetor, "%d", c->chaves[0]);
    for(int i = 1; i < c->tam; ++i){
        sprintf(elem, " %d", c->chaves[i]);
        strcat(vetor, elem);    
    }
    strcat(vetor, "\n");
    sprintf(string, "%c: ", c->elem);
    string[0] = tolower(string[0]);
    strcat(string, vetor);

    return string;
}
