#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "gerador.h"
#include "liblista.h"



lista_t *gera_lista_chaves(FILE* livro_cifras){
        int i = 0;
        char leitura[SIZE];
        lista_t* lista;

        lista = cria_lista();

        fscanf(livro_cifras, "%s", leitura);
        while(!feof(livro_cifras)){        
                adiciona_ordem_lista(lista, leitura[0], i);
                ++i;
                fscanf(livro_cifras, "%s", leitura);
        }

        return lista;
}


int gera_arquivo_chaves(FILE* arquivo_chaves, lista_t *lista){
        char* saida;
        nodo_t *nodo;
        nodo = lista->primeiro;

        while(nodo != NULL){
                saida = caracter_e_chaves(nodo);
                fputs(saida,arquivo_chaves);
                free(saida);
                nodo = nodo->prox;
        }

        return 1;
}
