#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv){

	int count = 0;

	for(int i = 0; i < argc; ++i){

		if(argv[i][0] == '-')
			count +=1;
	}

	return count;
}
