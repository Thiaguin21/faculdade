#include <stdlib.h>
#include <stdio.h>
#define SIZE 500

int main(){
	int indice[1];
	char leitura[2];
	char string[SIZE];
	FILE* entrada;

	entrada = fopen("poema.bin", "r");

	if(!entrada){
		perror("Erro ao abrir o arquivo poema.bin");
		exit(1);
	}

	fread(indice, sizeof(int), 1, entrada);
	fread(leitura, sizeof(char), 1, entrada);
	while(!feof(entrada)){
		string[indice[0]] = leitura[0];
		fread(indice, sizeof(int), 1, entrada);
		fread(leitura, sizeof(char), 1, entrada);
	}
	
	printf("%s\n", string);
	
	fclose(entrada);

	return 0;
}