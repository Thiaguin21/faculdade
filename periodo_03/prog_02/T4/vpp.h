struct dirent{
	char d_name[]; //nome da entrada.
	unsigned char d_namlen; //tamanho do nome, sem incluir o “\0” final.
	ino_t d_fileno; //número do i-node da entrada.
	unsigned char d_type; //tipo do arquivo.
};
