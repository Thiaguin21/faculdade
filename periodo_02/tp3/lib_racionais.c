#include <stdio.h>
#include <stdlib.h>

typedef struct racional {
    int num;
    int den;
} racional;

/* GERA A SEMETE ALEATÓRIA */
void srand();

/* ADEQUA O VALOR RANDÔMICO AO INTERVALO DEFINIDO */
int aleat (int min, int max){
	return ((rand() % (max - min + 1)) + min);
}

/* ALOCA MEMÓRIA PARA UM RACIONAL*/
racional *criar_r (){
	malloc(sizeof(racional));
	return criar_r;
}

/* LIBERA A MEMÓRIA DE UM RACIONAL ALOCADO */
racional *liberar_r (racional *r){
	free (r);
	r = NULL;
	return r;
}

/*  */
racional *sortear_r (){
	*criar_r ();
	*aleat ();

}

racional main(){
	*sortear_r();
	return 0;
}

