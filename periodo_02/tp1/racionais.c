/* BIBLIOTECAS */
#include <stdio.h>
#include <math.h>

/* VARIÁVEIS GLOBAIS*/
typedef struct racionaL{
  int n;
  int d;
}t_racional;

t_racional a;
t_racional b;

/* FUNÇÃO DE LEITURA */
t_racional leitura(){
  t_racional x;
  scanf("%d", &x.n);
  scanf("%d", &x.d);
  return x;
}

/* MDC */
int mdc(t_racional m){
  int x, n;
  if(m.n < m.d){
    n = m.d;
    m.d = m.n;
    m.n = n;
  }
  while(m.d != 0){
    n = m.n % m.d;
    m.n = m.d;
    m.d = n;
  }
  x = m.n;
  return x;
}

/* SIMPLIFICAÇÂO */
t_racional simplificacao(t_racional x){
  t_racional sp;
  int k = mdc(x);
  sp.n = x.n / k;
  sp.d = x.d / k;
  return sp;
}

/* FUNÇÃO PARA IMPRESSÃO */
void imprime(t_racional p1){
 if(p1.d < 0){
   p1.n = p1.n * (-1);
   p1.d = p1.d * (-1);
 }
 if(p1.d == 1) printf("%d\n", p1.n);
  else
    if(p1.d == 0) printf("%s\n", " ");
      else
        printf("%d/%d\n", p1.n, p1.d);
}

/* FUNÇÃO DE SOMA */
void soma(t_racional s1, t_racional s2){
  t_racional sm;
  sm.n = (s1.n * s2.d) + (s2.n * s1.d);
  sm.d = s1.d * s2.d;
  sm = simplificacao(sm);
  imprime(sm);
}

/* FUNÇÃO DE SUBTRAÇÃO */
void subtracao(t_racional s1, t_racional s2){
  t_racional sb;
  sb.n = (s1.n * s2.d) - (s2.n * s1.d);
  sb.d = s1.d * s2.d;
  sb = simplificacao(sb);
  imprime(sb);
}

/* FUNÇÃO DE MULTIPLIÇÃO */
void multiplicacao(t_racional s1, t_racional s2){
  t_racional mt;
  mt.n = s1.n * s2.n;
  mt.d = s1.d * s2.d;
  mt = simplificacao(mt);
  imprime(mt);
}

/* FUNÇÃO DE DIVISÃO */
void divisao(t_racional s1, t_racional s2){
  t_racional dv;
  dv.n = s1.n * s2.d;
  dv.d = s1.d * s2.n;
  dv = simplificacao(dv);
  imprime(dv);
}

/* PROGRAMA PRINCIPAL */
int main()
{
  printf("Digite dois Pares de Numeros\n");
  a = leitura();
  if(a.d == 0) return 0;
  b = leitura();
  while(a.d != 0 && b.d != 0 ){
    printf("\n");
    soma(a, b);
    subtracao(a, b);
    multiplicacao(a, b);
    divisao(a, b);
    printf("\nDigite dois Pares de Numeros\n");
    a = leitura();
    if(a.d == 0) break;
    b = leitura();
  }
  return 0;
}
