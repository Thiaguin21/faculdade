
#include <stdio.h>
#include <stdlib.h>
#include "lib_conjunto.h"

/*
 * Cria um conjunto vazio e o retorna, se falhar retorna NULL.
 * max eh o tamanho maximo do conjunto, isto eh, o tamanho maximo do vetor
 */
conjunto_t *cria_cjt (int max){
    
    conjunto_t *c;
    if(!(c = malloc(sizeof(conjunto_t)))) return NULL;
    
    c->max = max;
    c->card = 0;
    c->ptr = 0;
    
    if(!(c->v = malloc(sizeof(int) * c->max))) return NULL;
    
    for (int i = 0; i < c->max; ++i) c->v[i] = 0;

    return c;
}

/*
 * Remove todos os elementos do conjunto, libera espaco e devolve NULL.
 */
conjunto_t *destroi_cjt (conjunto_t *c){
    
    for(c->ptr = 0; c->ptr < c->card; c->ptr++) c->v[c->ptr] = 0; 
    
    c->max = 0;
    c->card = 0;
    c->ptr = 0;
    
    free(c->v);
    free(c);
    
    c->v = NULL;
    c = NULL;
    
    return c;
}

/*
 * Retorna 1 se o conjunto esta vazio e 0 caso contrario.
 */
int vazio_cjt (conjunto_t *c){
    
    if(cardinalidade_cjt(c) == 0) return 1;
    else return 0;
}

/*
 * Retorna a cardinalidade do conjunto, isto eh, o numero de elementos presentes nele.
 */
int cardinalidade_cjt (conjunto_t *c){

    return c->card;
}

/*
 * Insere o elemento no conjunto, garante que nao existam repeticoes.
 * Retorna 1 se a operacao foi bem sucedida. Se tentar inserir elemento existente,
 * nao faz nada e retorna 1 tambem. Retorna 0 em caso de falha por falta de espaco.
 */
int insere_cjt (conjunto_t *c, int elemento){
    
    int aux = 0;
    inicia_iterador_cjt(c);
        
    if(cardinalidade_cjt(c) == c->max) return 0;
        
    else if(!vazio_cjt(c)){
            
        while(c->ptr < c->max){

            if(elemento == c->v[c->ptr]) return 1;
            
            else if(c->ptr == cardinalidade_cjt(c)){
                
                c->v[c->ptr] = elemento;
                c->card++;
                return 1;
            }

            else if(elemento < c->v[c->ptr] && c->ptr < cardinalidade_cjt(c)){
                
                aux = c->v[c->ptr];
                c->v[c->ptr] = elemento;
                elemento = aux;
            }
        
            else if(c->ptr < cardinalidade_cjt(c)) c->ptr++;
             
        }
    }

    else{

        c->v[c->ptr] = elemento;
        c->card++;
        return 1;
    }
}


/*
 * Remove o elemento 'elemento' do conjunto caso ele exista.
 * Retorna 1 se a operacao foi bem sucedida e 0 se o elemento nao existe.
 */
int retira_cjt (conjunto_t *c, int elemento){
    
    int j =0;
    c->ptr = 0;
    
    while(c->ptr < c->card){
    
        if(c->v[c->ptr] != elemento) c->ptr++;
    
        else{ 
    
            for(j = c->ptr; j < c->card; j++){
    
                if(j != c->card -1) c->v[j] = c->v[j+1];
    
                else{
    
                    c->v[j] = 0;
                    c->card--;  
                }  
            }
    
            return 1;
        }
    }
    
    return 0;
}

/*
 * Retorna 1 se o elemento pertence ao conjunto e 0 caso contrario.
 */
int pertence_cjt (conjunto_t *c, int elemento){
    
    c->ptr = 0;    
    
    while(c->ptr < c->card){
    
        if(c->v[c->ptr] != elemento) c->ptr++;
        else return 1;
    }
    
    return 0;
}
        
/*
 * Retorna 1 se o conjunto c1 esta contido no conjunto c2 e 0 caso contrario.
 */
int contido_cjt (conjunto_t *c1, conjunto_t *c2){
    
    int control = 0;
    
    if(c1->card < c2->card){
    
        for(c1->ptr = 0; c1->ptr < c1->card; c1->ptr++){
    
            for(c2->ptr = 0; c2->ptr < c2->card; c2->ptr++){
    
                if(c1->v[c1->ptr] == c2->v[c2->ptr]){
    
                    control++;
                    break;
                }
            }
        }
    
        if(control == c1->card) return 1;
    }
    
    return 0;
}

/*
 * Retorna 1 se o conjunto c1 eh igual ao conjunto c2 e 0 caso contrario.
 */
int sao_iguais_cjt (conjunto_t *c1, conjunto_t *c2){
    
    c1->ptr = 0;
    
    if(c1->card == c2->card){
        
        while(c1->ptr < c1->card){
           
            if(c1->v[c1->ptr] != c2->v[c1->ptr]) return 0;
            else c1->ptr++;
        }

        return 1;    
    }

    return 0;
}

/*
 * Cria e retorna o conjunto diferenca entre c1 e c2, nesta ordem.
 * Retorna NULL se a operacao falhou.
 */
conjunto_t *diferenca_cjt (conjunto_t *c1, conjunto_t *c2){
    
    conjunto_t *dif;
    dif = copia_cjt(c1);
    
    for(c1->ptr = 0; c1->ptr < c1->card; c1->ptr++){
    
        for(c2->ptr = 0; c2->ptr < c2->card; c2->ptr++){
    
            if(c1->v[c1->ptr] == c2->v[c2->ptr]) retira_cjt(dif, c1->v[c1->ptr]);
        }
    }
    
    return dif;
}       

/*
 * Cria e retorna o conjunto interseccao entre os conjuntos c1 e c2.
 * Retorna NULL se a operacao falhou.
 */
conjunto_t *interseccao_cjt (conjunto_t *c1, conjunto_t *c2){

    conjunto_t *inter;
    inter = cria_cjt(c1->max);

    for(c1->ptr = 0; c1->ptr < c1->card; c1->ptr++){

        for(c2->ptr = 0; c2->ptr < c2->card; c2->ptr++){

            if(c1->v[c1->ptr] == c2->v[c2->ptr]) insere_cjt(inter, c1->v[c1->ptr]);
        }
    }

    return inter;
}

/*
 * Cria e retorna o conjunto uniao entre os conjunto c1 e c2.
 * Retorna NULL se a operacao falhou.
 */
conjunto_t *uniao_cjt (conjunto_t *c1, conjunto_t *c2){
    
    conjunto_t *uniao;
    uniao = cria_cjt(cardinalidade_cjt(c1)+cardinalidade_cjt(c2));

    for(int i = 0; i < cardinalidade_cjt(c1); i++) insere_cjt(uniao, c1->v[i]); 

    for(int j = 0; j < cardinalidade_cjt(c2); j++) insere_cjt(uniao, c2->v[j]);

    return uniao;
}

/*
 * Cria e retorna uma copia do conjunto c e NULL em caso de falha.
 */
conjunto_t *copia_cjt (conjunto_t *c){
    
    conjunto_t *copia;
    copia = cria_cjt(c->max);
    
    for(c->ptr = 0; c->ptr < c->card; c->ptr++) insere_cjt(copia, c->v[c->ptr]);
    
    return copia;
}


/*
 * Cria e retorna um subconjunto com elementos aleatorios do conjunto c.
 * Se o conjunto for vazio, retorna um subconjunto vazio.
 * Se n >= cardinalidade (c) entao retorna o proprio conjunto c.
 * Supoe que a funcao srand () tenha sido chamada antes.
 */
conjunto_t *cria_subcjt_cjt (conjunto_t *c, int n){
    
    conjunto_t *sub;
    int min, max;
    
    if(n >= c->card) sub = copia_cjt(c);
    
    else{

        sub = cria_cjt(n);

        min = c->v[0];
        max = c->v[c->card-1];
    
        for(sub->ptr = 0; sub->ptr < sub->max; sub->ptr++) insere_cjt(sub, rand() % (max - min + 1) + min);    
    } 
    
    return sub;
}

/*
 * Imprime os elementos do conjunto sempre em ordem crescente,
 * mesmo que a estrutura interna nao garanta isso.
 * Na impressao os elementos sao separados por um unico espaco
 * em branco entre os elementos, sendo que apos o ultimo nao
 * deve haver espacos em branco. Ao final imprime um \n.
 */
void imprime_cjt (conjunto_t *c){
    
    if(vazio_cjt(c)) printf("\n");

    else{
    
        for (int i = 0; i < cardinalidade_cjt(c); i++){

            if(i < cardinalidade_cjt(c)-1) printf("%d ", c->v[i]);
            else printf("%d\n", c->v[i]);
    
        }
    }

}

/*
 * As funcoes abaixo implementam um iterador que vao permitir
 * percorrer os elementos do conjunto.
 * O ponteiro ptr da struct (iterador) pode ser inicializado para apontar 
 * para o primeiro elemento e incrementado ate' o ultimo elemento 
 * do conjunto.
 */

/*
 * Inicializa ptr usado na funcao incrementa_iterador 
 */
void inicia_iterador_cjt (conjunto_t *c){
    
    c->ptr = 0;
}

/*
 * Devolve no parametro ret_iterador o elemento apontado e incrementa o iterador.
 * A funcao retorna 0 caso o iterador ultrapasse o ultimo elemento, ou retorna
 * 1 caso o iterador aponte para um elemento valido (dentro do conjunto).
 */
int incrementa_iterador_cjt (conjunto_t *c, int *ret_iterador){

    ret_iterador = &c->v[c->ptr];
    c->ptr++;

    if(c->ptr <= cardinalidade_cjt(c)) return 1;
    return 0;

}

/*
 * Escolhe um elemento qualquer do conjunto para ser removido, o remove e
 * o retorna.
 * Nao faz teste se conjunto eh vazio, deixa para main fazer isso       
 */
int retira_um_elemento_cjt (conjunto_t *c){

    int min, max, r;
    
    min = c->v[0];
    max = c->v[cardinalidade_cjt(c)-1];

    c->ptr = rand() % (max - min + 1) + min;
    r = c->v[c->ptr];
    retira_cjt(c, r);
    c->card--;

    return r;
}
