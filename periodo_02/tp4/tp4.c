#include <stdio.h>
#include <stdlib.h>
#include "lib_conjunto.h"
#define MAX 5

int main (){

    srand(0);


    /* ler os herois e suas habilidades */

    conjunto_t **h = NULL;
    int i1, r1 = 0;

    h = malloc((sizeof(conjunto_t*)*MAX));
    
    for(i1 = 0; i1 < MAX; i1++){
        
        h[i1] = cria_cjt(MAX);
        
        while(i1 < MAX){

            r1 = scanf("%d", &r1);
            
            if(r1 != 0) insere_cjt(h[i1], r1);
            else break;

            i1++;
        }

    }


    /* ler a missao */

    conjunto_t *m = NULL;
    int i2, r2 = 0;

    m = cria_cjt(MAX);
    
    while(i2 < MAX){

            r2 = scanf("%d", &r2);
            
            if(r2 != 0) insere_cjt(h[i2], r2);
            else break;

            i2++;
        }


    /* ler as equipes de herois */
    
    conjunto_t **t = NULL;
    int i3, r3 = 0;

    t = malloc((sizeof(conjunto_t*)*MAX));
    
    for(i3 = 0; i3 < MAX; i3++){
        
        t[i3] = cria_cjt(MAX);
        
        while(i3 < MAX){

            r3 = scanf("%d", &r3);
            
            if(r3 != 0) insere_cjt(t[i3], r3);
            else break;

            i3++;
        }

    }

    /* a solucao eh encontrada se a missao esta contido na uniao das 
     * habilidades de uma equipe, mas tomando-se aquela de menor tamanho. */

    conjunto_t **u, *u_copia = NULL;
    int *pt = NULL;
    int i, j = 0;

    u = malloc((sizeof(conjunto_t*)*MAX));
    u_copia = cria_cjt(MAX);

    for (i = 0; i < MAX; i++){
    
        inicia_iterador_cjt(t[i]);
        j = 0;

        while(j <= cardinalidade_cjt(t[i])){

            incrementa_iterador_cjt(t[i], pt);

            u[i] = uniao_cjt(u_copia, h[*pt]);            
            u_copia = destroi_cjt(u_copia);
            u_copia = u[i];

            j++;
        }

    }

    int k = 0;
    conjunto_t *resultado = NULL;

    for(k = 0; k < MAX; k++){
        
        if(contido_cjt(m, u[k])){
        
            if(resultado == NULL) resultado = t[k];

            else if(cardinalidade_cjt(resultado) > cardinalidade_cjt(t[k])) resultado = t[k];
        }

    }
    
    if(resultado == NULL) printf("NENHUMA");
    else imprime_cjt(resultado);


    /* libera toda a memoria alocada dinamicamente */

    int l = 0;

    for (l = 0; l < MAX; ++l){
        
        h[l] = destroi_cjt(h[l]);
        t[l] = destroi_cjt(t[l]);
        u[l] = destroi_cjt(u[l]);
    }

    u_copia = destroi_cjt(u_copia);
    m = destroi_cjt(m);
    
    free(h);
    h = NULL;

    free(t);
    t = NULL;

    free(u);
    u = NULL; 

    return 0;
}
