/* BIBLIOTECAS */
#include <stdio.h>
#include "lib_racionais.h"

t_racional a;
t_racional b;

/* PROGRAMA PRINCIPAL */
int main(){
    a = leitura();
    if(a.d == 0) return 0;
    b = leitura();
    while(a.d != 0 && b.d != 0 ){
        soma(a, b);
        subtracao(a, b);
        multiplicacao(a, b);
        divisao(a, b);
        printf("\n");
        a = leitura();
        if(a.d == 0) break;
        b = leitura();
    }
    return 0;
}
