/* VARIÁVEIS GLOBAIS*/
typedef struct racionaL{
    int n;
    int d;
}t_racional;

/* FUNÇÃO DE LEITURA */
t_racional leitura();

/* FUNÇÃO DE SOMA */
void soma(t_racional s1, t_racional s2);

/* FUNÇÃO DE SUBTRAÇÃO */
void subtracao(t_racional s1, t_racional s2);

/* FUNÇÃO DE MULTIPLIÇÃO */
void multiplicacao(t_racional s1, t_racional s2);

/* FUNÇÃO DE DIVISÃO */
void divisao(t_racional s1, t_racional s2);

