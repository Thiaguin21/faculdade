/* BIBLIOTECAS */
#include <stdio.h>
#include "lib_racionais.h"

/* FUNÇÃO DE LEITURA */
t_racional leitura(){
    t_racional x;
    scanf("%d", &x.n);
    scanf("%d", &x.d);
    return x;
}

/* MDC */
int mdc(int m1, int m2){
    int a, b;
    a = m1;
    b = m2;
    if(b == 0) return a;
    else return mdc(b, a%b);
}

/* SIMPLIFICAÇÂO */
t_racional simplificacao(t_racional x){
    t_racional sp;
    int k = mdc(x.n, x.d);
    sp.n = x.n / k;
    sp.d = x.d / k;
    return sp;
}

/* FUNÇÃO PARA IMPRESSÃO */
void imprime(t_racional p1){
    if(p1. n == 0) printf("%d\n", 0);
    else if(p1.d == 1) printf("%d\n", p1.n);
        else if(p1.n == p1.d) printf("%d\n", 1);
            else printf("%d/%d\n", p1.n,p1.d);
}

/* FUNÇÃO DE SOMA */
void soma(t_racional s1, t_racional s2){
    t_racional sm;
    sm.n = (s1.n * s2.d) + (s2.n * s1.d);
    sm.d = s1.d * s2.d;
    sm = simplificacao(sm);
    imprime(sm);
}

/* FUNÇÃO DE SUBTRAÇÃO */
void subtracao(t_racional s1, t_racional s2){
    t_racional sb;
    sb.n = (s1.n * s2.d) - (s2.n * s1.d);
    sb.d = s1.d * s2.d;
    sb = simplificacao(sb);
    imprime(sb);
}

/* FUNÇÃO DE MULTIPLIÇÃO */
void multiplicacao(t_racional s1, t_racional s2){
    t_racional mt;
    mt.n = s1.n * s2.n;
    mt.d = s1.d * s2.d;
    mt = simplificacao(mt);
    imprime(mt);
}

/* FUNÇÃO DE DIVISÃO */
void divisao(t_racional s1, t_racional s2){
    t_racional dv;
    dv.n = s1.n * s2.d;
    dv.d = s1.d * s2.n;
    dv = simplificacao(dv);
    imprime(dv);
}
