public class FuncionarioHorista extends Funcionario{
	private float valorH;
	private int numeroH;

	public FuncionarioHorista(){
		super();
	}

	public FuncionarioHorista(String nome, int dia, int mes, int ano, boolean ehEst, float valorH, int numeroH){
		super(nome, dia, mes, ano, ehEst);
		this.setValorH(valorH);
		this.setNumeroH(numeroH);
	}

	public FuncionarioHorista(String nome, int valorH, int numeroH){
		this(nome, 0, 0, 0, false, valorH, numeroH);
	}

	public void setValorH(float valorH){
		if(valorH > 0){
			this.valorH = valorH;	
		}
	}

	public void setNumeroH(int numeroH){
		if(numeroH > 0){
			this.numeroH = numeroH;	
		}
	}

	public float getValorH(){
		return this.valorH;	
	}

	public int getNumeroH(){
		return this.numeroH;	
	}

	public String toString(){
		String dados, dadosComp;

		dados = super.toString();
		dadosComp = (dados + "valorH: " + this.valorH + "\n" + "numeroH: " + this.numeroH + "\n");

		return dadosComp;
	}
}