public class FuncionarioMensalista extends Funcionario{
	private float salarioM;

	public FuncionarioMensalista(){
		super();
	}

	public FuncionarioMensalista(String nome, int dia, int mes, int ano, boolean ehEst, float salarioM){
		super(nome, dia, mes, ano, ehEst);
		this.setSalario(salarioM);
	}

	public FuncionarioMensalista(String nome, float salarioM){
		this(nome, 0, 0, 0, false, salarioM);
	}

	public void setSalario(float salarioM){
		this.salarioM = salarioM;
	}

	public float getSalario(){
		return this.salarioM;
	}

	public String toString(){
		String dados, dadosComp;

		dados = super.toString();
		dadosComp = (dados + "salarioM: " + this.salarioM + "\n");

		return dadosComp;
	}
}