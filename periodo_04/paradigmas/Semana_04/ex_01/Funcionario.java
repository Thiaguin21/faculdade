public class Funcionario{
	protected String nome;
	protected Data contrat;
	protected boolean ehEst;

	public Funcionario(){}

	public Funcionario(String nome, int dia, int mes, int ano, boolean ehEst){
		this.setNome(nome);
		this.setContrat(dia, mes, ano);
		this.setEhEst(ehEst);
	}

	public void setNome(String nome){
		this.nome = nome;
	}

	public void setContrat(int dia, int mes, int ano){
		this.contrat = new Data(dia, mes, ano);
	}

	public void setEhEst(boolean ehEst){
		this.ehEst = ehEst;
	}

	public String getNome(){
		return this.nome;
	}

	public Data getContrat(){
		return this.contrat;
	}

	public boolean getEhEst(){
		return this.ehEst;
	}

	public String toString(){
		String data, dados;

		data = contrat.toString();
		dados = ("nome: " + this.nome + "\n" + "contratacao: " + data + "eH Estrang: " + this.ehEst);
		
		return dados;
	}
}