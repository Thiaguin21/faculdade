public class Data{
	private int dia;
	private int mes;
	private int ano;

	public Data(){}

	public Data(int dia, int mes, int ano){
		this.setDia(dia);
		this.setMes(mes);
		this.setAno(ano);
	}

	public void setDia(int dia){
		if((dia > 0) && (dia < 32)){
			this.dia = dia;
		}
	}

	public void setMes(int mes){
		if((mes > 0) && (mes < 13)){
			this.mes = mes;
		}
	}

	public void setAno(int ano){
		if(ano > 0){
			this.ano = ano;
		}
	}

	public int getDia(){
		return this.dia;
	}

	public int getMes(){
		return this.mes;
	}

	public int getAno(){
		return this.ano;
	}

	public String toString(){
		String data;
		data = (this.dia + "/" + this.mes + "/" + this.ano + "\n");
	
		return data;
	}
}