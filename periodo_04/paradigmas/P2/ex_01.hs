module Exercicios where

type Nome = String
type Titulo = String
type Genero = Char
type Pesquisador = (Nome, Titulo, Genero)
type Grupo = [Pesquisador]

base :: Int -> Pesquisador
base x
 | x == 1 = ("joao", "mestre", 'm')
 | x == 2 = ("jonas", "doutor", 'm')
 | x == 3 = ("joice", "mestre", 'f')
 | x == 4 = ("janete", "doutor", 'f')
 | x == 5 = ("jocileide", "doutor", 'f')
 | otherwise = ("niguem", "", 'x')

pesquisadores :: [Pesquisador]
pesquisadores = listaPesquisador (5)

elemento1 :: Pesquisador -> Nome
elemento1 (s1, s2, c) = s1

elemento2 :: Pesquisador -> Titulo 
elemento2 (s1, s2, c) = s2

elemento3 :: Pesquisador -> Genero 
elemento3 (s1, s2, c) = c

contMestre :: Int -> Int
contMestre 0 = 0
contMestre n
 | elemento2(base n) == "mestre" = 1 + contMestre (n-1)
 | otherwise = contMestre (n-1)

contDoc :: Int -> Int
contDoc 0 = 0
contDoc n
 | elemento2(base n) == "doutor" = 1 + contDoc (n-1)
 | otherwise = contDoc (n-1)

contMD :: Int -> String -> Int
contMD 0 title = 0
contMD n title
 | elemento2(base n) == title = 1 + contMD (n-1) (title)
 | otherwise = contMD (n-1) (title)

cont :: Int -> Char -> Int
cont 0 g = 0
cont n g
 | elemento3(base n) == g = 1 + cont (n-1) (g)
 | otherwise = cont (n-1) (g)

listaPesquisador :: Int -> Grupo
listaPesquisador 0 = []
listaPesquisador n = [base(n)] ++ listaPesquisador(n-1)

nomesPesquisadores :: [Pesquisador] -> [Nome]
nomesPesquisadores [] = []
nomesPesquisadores (h:t) = [elemento1(h)] ++ nomesPesquisadores(t)

adionaSr :: [Pesquisador] -> ([Pesquisador] -> [Nome]) -> [Nome] 
adionaSr = 