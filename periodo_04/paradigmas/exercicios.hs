module Exercicios where

lista1 :: [Int]
lista1 = [1..5]

lista2 :: [Float]
lista2 = [1..5]

lista3 :: [String]
lista3 = ["Ana", "Beatriz", "Joaquim", "Anabeth"]

lista4 :: [Int]
lista4 = [3*x | x <- [0..5]]

lista5 :: [[Int]]
lista5 = [[x] | x <- [0..5]]

somaElementosLista :: [Int] -> Int
somaElementosLista [] = 0
somaElementosLista (h:t) = h + somaElementosLista t

removePares :: [Int] -> [Int]
removePares [] = []
removePares (h:t)
 | h `mod` 2 == 0 = removePares t 
 | otherwise =  [h] ++ removePares t

inverteLista :: [Float] -> [Float]
inverteLista [] = []
inverteLista (h:t) = inverteLista t ++ [h]

filtraNomes :: [String] -> [String]
filtraNomes [] = []
filtraNomes (h:t)
 | head h == 'A' = [h] ++ filtraNomes t
 | otherwise = filtraNomes t

retornaTupla :: Int -> (Int, Int, Int, Int)
retornaTupla e = (a, b, c, d)
 where
  a = 2*e
  b = 3*e
  c = 4*e
  d = 5*e

ehParImpar :: Int -> (Int, String)
ehParImpar e
 | e `mod` 2 =
 | otherwise =