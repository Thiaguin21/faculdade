module Recursao where

fibonacci :: Int -> Int
fibonacci n
 | n == 0 = 0
 | n == 1 = 1
 | otherwise = fibonacci(n-1) + fibonacci(n-2)

numeroDigitos :: Int -> Int
numeroDigitos n
 | n == 0 = 0
 | otherwise = 1 + numeroDigitos(abs(n) `div` 10)

somaDigitos :: Int -> Int
somaDigitos n
 | n == 0 = 0
 | otherwise = abs(n) `mod` 10 + somaDigitos(abs(n) `div` 10) 