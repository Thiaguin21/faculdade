module PrimeiroPrograma where

-- Definir escopo
num :: Int -> Float
-- Declarar funcao
num 1 = 5.00
num 2 = 10.00
num 3 = 15.00
num 4 = 20.00
num 5 = 25.00

-- Soma os fatores do numerador
soma :: Int -> Float
soma n
 | n == 0 = 0
 | otherwise = num(n) + soma(n-1)

-- Realiza a media
media :: Int -> Float
media n = soma(n) / fromIntegral(n)