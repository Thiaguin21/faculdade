module Exercicios where

alcoolAprovsV1 :: Int -> String
alcoolAprovsV1 a
 | a >= 18 = "Aproved"
 | otherwise = "Disaproved"

alcoolAprovsV2 :: Int -> String
alcoolAprovsV2 a = if a >= 18
 then "Aproved"
 else "Disaproved"

numberSignalV1 :: Int -> String
numberSignalV1 a
 | a == 0 = "Zero"
 | a > 0 = "Positive"
 | otherwise = "Negative"

numberSignalV2 :: Int -> String
numberSignalV2 a = if a == 0
 then "Zero"
 else if (a > 0)
 then "Positive"
 else "Negative"

ehBissextoV1 :: Int -> String
ehBissextoV1 a
 = if (a `mod` 400) == 0
    then "eh Bissexto"
    else if (a `mod` 4) == 0
     then if (a `mod` 100) == 0
      then "nao eh Bissexto"
      else "eh Bissexto"
     else "nao eh Bissexto"

ehBissextoV2 :: Int -> String
ehBissextoV2 a
 = if (a `mod` b) == 0
    then "eh Bissexto"
    else if ((a `mod` c) == 0) && not((a `mod` d) == 0)
      then "eh Bissexto"
      else "nao eh Bissexto"
   where
      b = 400
      c = 4
      d = 100

ehBissextoV3 :: Int -> String
ehBissextoV3 a
 | (a `mod` b) == 0 = "eh Bissexto"
 | ((a `mod` c) == 0) && not((a `mod` d) == 0) = "eh Bissexto"
 | otherwise = "nao eh Bissexto"
 where
  b = 400
  c = 4
  d = 100