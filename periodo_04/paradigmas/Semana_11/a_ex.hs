module Exercicios where

reajustarSalario :: Float -> Float
reajustarSalario x = x * 1.25

mediaPonderada :: Float -> Float -> Float -> Float -> Float -> Float -> Float
mediaPonderada n1 p1 n2 p2 n3 p3 = (n1*p1 + n2*p2 + n3*p3) * 1/3

converterTemp :: Float -> Float
converterTemp c = c*(9/5)+32

calcularIdade2032 :: Int -> Int
calcularIdade2032 idade = 2032 - idade

conveterParaMin :: Int -> Int -> Int
conveterParaMin hora min = hora*60 + min

calcularSomaQuadradosV1 :: Float -> Float -> Float -> Float
calcularSomaQuadradosV1 a1 a2 a3 = a1*a1 + a2*a2 + a3*a3

calcularSomaQuadradosV2 :: Float -> Float -> Float -> Float
calcularSomaQuadradosV2 a1 a2 a3 = a1**2 + a2**2 + a3**2

calcularSomaQuadradosV3 :: Int -> Int -> Int -> Int
calcularSomaQuadradosV3 a1 a2 a3 = a1^a1 + a2^a2 + a3^a3

ehImparV1 :: Int -> Boolean
ehImparV1
iseven num = mod num 2 
if num == 0
then False
else True