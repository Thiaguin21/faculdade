public class Time{
	private int minuto;
	private int hora;

	public Time(){
		this.minuto = 0;
		this.hora = 0;
	};

	public Time(int minuto){
		this.setTimeMinuto(minuto);
	};

	public Time(int minuto, int hora){
		this.setTimeMinuto(minuto);
		this.setTimeHora(hora);
	};

	public 	int getTimeMinuto(){
		return this.minuto;
	}

	public 	int getTimeHora(){
		return this.hora;
	}

	public 	void setTimeMinuto(int minuto){

		if((minuto < 0) || (minuto > 59)) {
			this.minuto = 0;
		}
		else{
			this.minuto = minuto;
		}	
	}

	public 	void setTimeHora(int hora){

		if((hora < 0) || (hora > 23)) {
			this.hora = 0;
		}
		else{
			this.hora = hora;
		}
	}

	public void incrementaTime(){
		int contador;
		contador = this.getContador();
		this.setContador(contador + 1);
	}

	public void decrementaTime(){
		int contador;
		contador = this.getContador();
		this.setContador(contador - 1);
	}

}