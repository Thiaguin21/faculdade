
import java.util.Scanner;

public class Principal{
	public static void main(String[] args) {
		Contador a = new Contador();
		Contador b = new Contador(3);
		Scanner leitura = new Scanner(System.in);
		int ent, impressao;

		System.out.println("Digite um tamanho:");
		ent = leitura.nextInt();

		impressao = a.getContador();
		System.out.printf("Contador a: %d\n",impressao);

		impressao = b.getContador();
		System.out.printf("Contador b: %d\n",impressao);
		

		System.out.println("Contador a: INCREMENTA");
		System.out.println("Contador b: DECREMENTA");

		for (int i = 0; i < ent; ++i) {
			a.incrementaContador();
			b.decrementaContador();

			impressao = a.getContador();
			System.out.printf("Contador a: %d\n",impressao);

			impressao = b.getContador();
			System.out.printf("Contador b: %d\n",impressao);
		}

		System.out.print("Contador a: ZERA\n");
		a.zeraContador();

		impressao = a.getContador();
		System.out.printf("Contador a: %d\n",impressao);
	}
}