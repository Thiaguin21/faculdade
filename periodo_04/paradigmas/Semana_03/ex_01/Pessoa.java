public class Pessoa{
	private String nome;
	private int idade;
	private int dia_n;
	private int mes_n;
	private int ano_n;

	public Pessoa(){
		this.nome = ' ';
		this.idade = 0;
		this.dia_n = 0;
		this.mes_n = 0;
		this.ano_n = 0;
	};

	public Pessoa(String n, int i, int d, int m, int a){
		this.setNome(n);
		this.setIdade(i);
		this.setDiaN(d);
		this.setMesN(m);
		this.setAnoN(a);
	};

	public 	String getNome(){
		return this.nome;
	}

	public 	int getIdade(){
		return this.idade;
	}

	public 	int getDiaN(){
		return this.dia_n;
	}

	public 	int getMesN(){
		return this.mes_n;
	}

	public 	int getAnoN(){
		return this.ano_n;
	}

	public 	void setNome(String n){
		this.nome = n;
	}

	public 	void setIdade(int i){
		if(i < 150){
			this.idade = i;
		}
	}

	public 	void setDiaN(String d){
		this.dia_n = d;
	}

	public 	void setMesN(String m){
		this.mes_n = m;
	}

	public 	void setAnoN(String a){
		this.ano_n = a;
	}
 

}