public class Professor {
	private String nome;
	private Integer matricula;

	public void setNome(String nome){
		this.nome = nome;
	}

	public String getNome(){
		return this.nome;
	}

	public void setMatricula(Integer matricula){
		this.matricula = matricula;
	}

	public Integer getMatricula(){
		return this.matricula;
	}

	Professor(){}

	Professor(String nome, Integer matricula){
		this.setNome(nome);
		this.setMatricula(matricula);
	}

}