public class ImovelVelho extends Imovel{

	ImovelVelho(){}

	ImovelVelho(String rua, Integer numero, String bairro, String cidade, String estado, float preco){
		super(rua, numero, bairro, cidade, estado, preco);
	}

	public float calcularValorImovel(){
		return this.preco * 0.95f;
	}
}