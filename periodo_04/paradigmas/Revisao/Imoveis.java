public class Imoveis{
	public static void main(String[] args) {
		Imovel[] imoveis = new Imovel[200];

		for (int i=0; i < 100; ++i) {
			imoveis[i] = new ImovelNovo("Rua" + i, i, "Bairro" + i, "Cidade" + i, "Estado" + i, 400.000f + i);
			imoveis[i+1] = new ImovelVelho("Rua" + i+1, i+1, "Bairro" + i+1, "Cidade" + i+1, "Estado" + i+1, 400.000f+ i);
		}

		for (int i=0; i < 200; ++i) {
			System.out.println(imoveis[i].calcularValorImovel());
		}


	}
}