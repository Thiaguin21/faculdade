import java.util.*;

public class Monitores {

	public static void main(String[] args) {
		
		TreeSet<Monitor> set = new TreeSet<Monitor>();
		
		Monitor mnt1 = new Monitor("Thiago", 1, "CI1062");
		Monitor mnt2 = new Monitor("Frank", 2, "CI182");
		Monitor mnt3 = new Monitor("Biehl", 3, "CI182");
		
		set.add(mnt1);
		set.add(mnt2);
		set.add(mnt3);

		for(Monitor m : set) {
			System.out.println(m.getNome() + " " + m.getMatricula() + " " + m.getCodigo());
		}
	}
}