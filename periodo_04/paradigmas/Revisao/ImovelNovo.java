public class ImovelNovo extends Imovel{

	ImovelNovo(){}

	ImovelNovo(String rua, Integer numero, String bairro, String cidade, String estado, float preco){
		super(rua, numero, bairro, cidade, estado, preco);
	}

	public float calcularValorImovel(){ 
		return this.preco * 1.25f;
	}
}