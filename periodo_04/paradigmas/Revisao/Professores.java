import java.util.*;

public class Professores {

	public void buscar(LinkedList<Professor> pfs, Professor pf){
		Boolean control = false;

		for (Professor p : pfs){
			if(pf.getNome().equals(p.getNome()))
				control = true;
		}

		if(control)
			System.out.println("Objeto encontrado!");

		else
			System.out.println("Objeto não encontrado!");

	}

	public static void main(String[] args) {
		
		LinkedList<Professor> professores = new LinkedList<Professor>();
		
		Professor pf1 = new Professor("pf1", 1);
		Professor pf2 = new Professor("pf2", 2);
		Professor pf3 = new Professor("pf3", 3);
		Professor pf4 = new Professor("pf4", 4);
		Professor pf5 = new Professor("pf5", 5);

		professores.add(pf1);
		professores.add(pf2);
		professores.add(pf3);
		professores.add(pf4);
		professores.add(pf5);

		buscar(professores, pf1);
		professores.remove(pf1);
		buscar(professores, pf1);
	}
}