public class Endereco{
	protected String rua;
	protected Integer numero;
	protected String bairro;
	protected String cidade;
	protected String estado;

	public void setRua(String rua){
		this.rua = rua;
	}

	public String getRua(){
		return this.rua;
	}

	public void setNumero(Integer numero){
		this.numero = numero;
	}

	public Integer getNumero(){
		return this.numero;
	}
	
	public void setBairro(String bairro){
		this.bairro = bairro;
	}

	public String getBairro(){
		return this.bairro;
	}

	public void setCidade(String cidade){
		this.cidade = cidade;
	}

	public String getCidade(){
		return this.cidade;
	}

	public void setEstado(String estado){
		this.estado = estado;
	}

	public String getEstado(){
		return this.estado;
	}

	Endereco(){}

	Endereco(String rua, Integer numero, String bairro, String cidade, String estado){
		this.setRua(rua);
		this.setNumero(numero);
		this.setBairro(bairro);
		this.setCidade(cidade);
		this.setEstado(estado);
	}
}