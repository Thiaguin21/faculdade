public abstract class Imovel{
	protected Endereco endereco;
	protected float preco;

	public void setPreco(float preco){
		this.preco = preco;
	}

	public float getPreco(){
		return this.preco;
	}

	Imovel(){}

	Imovel(String rua, Integer numero, String bairro, String cidade, String estado, float preco){
		this.endereco = new Endereco(rua, numero, bairro, cidade, estado);
		this.setPreco(preco);
	}

	abstract float calcularValorImovel();
}