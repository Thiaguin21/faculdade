public class Monitor implements Comparable<Monitor>{

	private String nome;
	private Integer matricula;
	private String codigo;

	public void setNome(String nome){
		this.nome = nome; 
	}

	public String getNome(){
		return this.nome;
	}

	public void setMatricula(Integer matricula){
		this.matricula = matricula; 
	}

	public Integer getMatricula(){
		return this.matricula;
	}

	public void setCodigo(String codigo){
		this.codigo = codigo; 
	}

	public String getCodigo(){
		return this.codigo;
	}

	Monitor(){}

	Monitor(String nome, Integer matricula, String codigo){
		this.setNome(nome);
		this.setMatricula(matricula);
		this.setCodigo(codigo);
	}

	public int compareTo(Monitor m){
		if(m.nome.charAt(0) < this.nome.charAt(0))
			return 1;
		else if(m.nome.charAt(0) > this.nome.charAt(0))
			return -1;
		else
			return 0;
	}
}