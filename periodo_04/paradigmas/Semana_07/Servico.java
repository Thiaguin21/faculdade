public class Servico implements Loja{
	
	char formato;
	float duracao;

	public void setFormato(char formato){
		if(formato == 'R' || formato == 'L' || formato == 'D')
			this.formato = formato;
	}

	public void setDuracao(float duracao){
		if(duracao > 0)
			this.duracao = duracao;
	}

	public char getFormato(){
		return this.formato;
	}

	public float getDuracao(){
		return this.duracao;
	}

	public Servico(){

	}

	public Servico(char formato, float duracao){
		this.setFormato(formato);
		this.setDuracao(duracao);
	}

	public void vender(){
		System.out.printf("Vendendo um Serviço!\n");
	}

	public void acionarSeguro(){
		System.out.printf("Seguro de um Serviço!\n");
	}
}