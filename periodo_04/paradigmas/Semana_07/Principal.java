import java.util.*;

public class Principal{

	public static void main(String[] args) {
		Loja c = new Celular("Algum", 15, "Alguma", "Cristal");
		Loja t = new Televisao("Algum", 15, "Alguma", 55);
		Loja s = new Servico('R', 90);
		ArrayList<Loja> a = new ArrayList<Loja> (2);

		a.add(c);
		a.add(t);
		a.add(s);

		for (int i=0; i < a.size(); ++i) {
			Loja k = a.get(i);

			k.vender();
			k.acionarSeguro();
			
			if(k instanceof Celular || k instanceof Televisao){
				k.ligar();
				k.desligar();
			}
		}
	}
}