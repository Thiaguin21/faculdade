public abstract class ProdutoEletronico implements Loja{

	String fabricante;
	float peso;
	String marca;

	public void setFabricante(String fabricante){
		if(fabricante != "")
			this.fabricante = fabricante;
	}

	public void setPeso(float peso){
		if(peso > 0)
			this.peso = peso;
	}

	public void setMarca(String marca){
		if(marca != "")
			this.marca = marca;
	}

	public String getFabricante(){
		return this.fabricante;
	}

	public float getPeso(){
		return this.peso;
	}

	public String getMarca(){
		return this.marca;
	}

	public ProdutoEletronico(){

	}

	public ProdutoEletronico(String fabricante, float peso, String marca){
		this.setFabricante(fabricante);
		this.setPeso(peso);
		this.setMarca(marca);
	}

	// public void vender(){
	// 	System.out.printf("Vendendo um Produto Eletrônico!\n");
	// }

	// public void acionarSeguro(){
	// 	System.out.printf("Seguro de um Produto Eletrônico!\n");
	// }

	public abstract void ligar();

	public abstract void desligar();

}