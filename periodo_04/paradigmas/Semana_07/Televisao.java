public class Televisao extends ProdutoEletronico{
	
	float polegada;

	public void setPolegada(float polegada){
		if(polegada > 0)
			this.polegada = polegada;
	}

	public float getPolegada(){
		return this.polegada;
	}

	public Televisao(){

	}

	public Televisao(String fabricante, float peso, String marca, float polegada){
		super(fabricante, peso, marca);
		this.setPolegada(polegada);
	}

	public void vender(){
		System.out.printf("Vendendo uma Televisao!\n");
	}

	public void acionarSeguro(){
		System.out.printf("Seguro de uma Televisao!\n");
	}

	public void ligar(){
		System.out.printf("Ligando Televisao!\n");	
	}

	public void desligar(){
		System.out.printf("Desligando Televisao!\n");
	}
}