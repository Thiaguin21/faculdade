public class Celular extends ProdutoEletronico{
	
	String tela;

	public void setTela(String tela){
		if(tela != "")
			this.tela = tela;
	}

	public String getTela(){
		return this.tela;
	}

	public Celular(){

	}

	public Celular(String fabricante, float peso, String marca, String tela){
		super(fabricante, peso, marca);
		this.setTela(tela);
	}

	public void vender(){
		System.out.printf("Vendendo um Celular!\n");
	}

	public void acionarSeguro(){
		System.out.printf("Seguro de um Celular!\n");
	}

	public void ligar(){
		System.out.printf("Ligando Celular!\n");	
	}

	public void desligar(){
		System.out.printf("Desligando Celular!\n");
	}
}